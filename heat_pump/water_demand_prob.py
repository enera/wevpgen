# =============================================================================
# Generate a static list Wasserbedarfswahrscheinlichkeit
# =============================================================================

def erstelleWasserbedarf():
    wasserbedarf = []
    i=0
    while i<50:
    	wasserbedarf.append(0.002)
    	i+=1
    
    wasserbedarf.append(0.04)
    
    i=51
    while i<70:
        wasserbedarf.append(wasserbedarf[i-1]+0.0045)
        i+=1
    
    wasserbedarf.append(0.13) #07:00
    
    i=71
    while i<85:
        wasserbedarf.append(wasserbedarf[i-1]-0.00614)
        i+=1
    
    wasserbedarf.append(0.038)
    
    i=86
    while i<120:
        wasserbedarf.append(wasserbedarf[i-1]+0.00005)
        i+=1
    
    wasserbedarf.append(0.04)
    
    i=121
    while i<175:
        wasserbedarf.append(wasserbedarf[i-1])
        i+=1
    
    wasserbedarf.append(0.04) #17:30
    
    i=176
    while i<192:
        wasserbedarf.append(wasserbedarf[i-1]+0.00265)
        i+=1
    
    wasserbedarf.append(0.085)
    
    i=193
    while i<210:
        wasserbedarf.append(wasserbedarf[i-1]-0.00265)
        i+=1
    
    wasserbedarf.append(0.04)
    
    i=211
    while i<230:
        wasserbedarf.append(wasserbedarf[i-1]-0.00015)
        i+=1
    
    wasserbedarf.append(0.037)
    
    i=231
    while i<240:
        wasserbedarf.append(0.002)
        i+=1
    
    return wasserbedarf
