import random
import numpy as np
from heat_pump.GaussVerteilung import fitte_gauss
from heat_pump.water_demand_prob import erstelleWasserbedarf
from tqdm import tqdm

class Waermepumpe(object):

    def __init__(self, Modell, anzPers, db_data):
        self.Modell = Modell
        self.anzPers = anzPers
        self.db_data = db_data
        self.wasserbedarf = erstelleWasserbedarf()
        self.BedarfswahrschproStunde = []
        
        self.P_WP=[]
        self.debugTInnen=[]

    
    #Auswahl der Modellparameter:    
    def Model_func(self):
        if (self.Modell==1):
            print("Einfamilienhaus")
            self.tempspezTansmissionsWV = 0.42         #Werte zwischen 0,2 (KfW Effizienzhaus 40), (0,5 EnEV Neubau standard), 0,95 (Altbau)
            self.Umfassungsflaeche = 475.6
            self.Bruttovolumen = 663.8
        
        elif (self.Modell==2):
            print("Zweifamilienhaus")
            self.tempspezTansmissionsWV =0.45          #Werte zwischen 0,2 (KfW Effizienzhaus 40), (0,5 EnEV Neubau standard), 0,95 (Altbau)
            self.Umfassungsflaeche = 402.9
            self.Bruttovolumen = 663.8
    
        elif (self.Modell==3):
            print("Mehrfamilienhaus")
            self.tempspezTansmissionsWV = 0.5
            self.Umfassungsflaeche = 200 
            self.Bruttovolumen = self.Umfassungsflaeche * 1.9 

    #konstanter Wasserbedarfswahrscheinlichkeit von 4 Personen auf anzPers skalieren:
    def erstelleBedarfswahrschproStunde(self): 
        #für 24h
        for r in range(240):
            self.BedarfswahrschproStunde.append(self.wasserbedarf[r] * (self.anzPers / 4))

    def berechneVolStromEnergie(self):
        data_temp = self.db_data.loc[:,'temperature']
        temperaturzeitreihe = [x for x in data_temp]
        anzStunden = len(temperaturzeitreihe)
        
        #Modellparameter:
        KaltWasserTemperatur = 10
        WarmWasserTemperatur = 50
        WPLeistung = 3 #in kW                      
        T_einschalt = 17
        T_ausschalt = 20
        COP = 3.0
        
        #Variablen initialisierung
        WP_Laeuft = False
        
        WpLZaehler = 0
        T_InnenZaehler = 0
        
        debugTInnen =[0] * anzStunden
        P_WP = [0] * anzStunden

        #Starttemperatur zufällig zwischen 18 und 22 ziehen:
        T_Innen = random.uniform(18,22)
        
        #Volumenstrom berechnen für einen Tag
        _, VolumenstromVert = fitte_gauss(15,1,240)  

        
        for i in tqdm(range(anzStunden)):
            #Stündliche Aggregation der 6-Minuten Intervalle:
            for ii in range(10): 
                t = i % 24
                if np.random.choice(np.arange(1, 3), p=[self.BedarfswahrschproStunde[t*10+ii], 1-self.BedarfswahrschproStunde[t*10+ii]]) == 1:
                    VolStrom = VolumenstromVert[t*10+ii]
                else:
                    VolStrom = 0
                
                # Berechne Energie aus Volumenstrom:
                WWEnergie = (WarmWasserTemperatur - KaltWasserTemperatur) * ((1.1625 * VolStrom) / 60)*6 #nur fürs Verständnis!

                #Zweipunktregelung - falls T_Innen<T_einschalt --> WP aktivieren
                if (T_Innen < T_einschalt or WWEnergie > 0):   
                    WP_Laeuft = True
            
                elif (T_Innen > T_ausschalt):
                    WP_Laeuft = False
                
                #Verluste für Temperaturdifferenz
                Delta_Temp = T_Innen - temperaturzeitreihe[i] 
                TransmissionsWaermeVerluste = self.tempspezTansmissionsWV * self.Umfassungsflaeche * Delta_Temp * 0.1
                
                if (WP_Laeuft == True and WWEnergie > 0):
                    zugWaermeQ = (0.1*COP * WPLeistung * 1000) - WWEnergie
                    if (zugWaermeQ < 0): #Fall WW braucht mehr Leistung als WP liefert
                        WWEnergie = zugWaermeQ *(-1)
                        zugWaermeQ = 0
                    else:
                        WWEnergie = 0
                elif (WP_Laeuft == True and WWEnergie <= 0):
                    zugWaermeQ = (0.1*COP * WPLeistung * 1000)
                else:
                    zugWaermeQ = 0
                
                #Wärmeenergie und neue resultierende Raumtemperatur:
                DeltaWaermeEnergie = zugWaermeQ - TransmissionsWaermeVerluste #in Watt
                Ewirk = 0.35*self.Bruttovolumen #(Wh / m³K * m³) für umrechnung in Kelvin
                T_diff = (DeltaWaermeEnergie / Ewirk) * 0.1
                T_Innen = T_Innen + T_diff
                
                #Output:
                if (WP_Laeuft == True):
                    WpLZaehler = WpLZaehler + (WPLeistung / 10)
                T_InnenZaehler = T_InnenZaehler + (T_Innen / 10)    
            P_WP[i] = WpLZaehler
            WpLZaehler = 0
            debugTInnen[i] = T_InnenZaehler
            T_InnenZaehler = 0            

        self.debugTInnen = debugTInnen
        self.P_WP = P_WP
        
    def wrapper(self):                
        self.Model_func()
        self.erstelleBedarfswahrschproStunde()
        self.berechneVolStromEnergie()