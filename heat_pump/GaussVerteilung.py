from math import pi, pow, sqrt, exp


def fitte_gauss(ub, lb, anzSchritte):
    sigma = (ub - lb) /3
    ew = ((ub - lb) /2 + lb)
    schrittweite = (ub - lb) / (anzSchritte - 1)
    
    yWerte = []
    dichteVerlauf = []
    
    #Calculate yWerte
    yWerte =[lb+schrittweite*i for i in range(anzSchritte)]
    
    #Determine Dichteverlauf and add to list dichteVerlauf
    dichteVerlauf = [
        (1 / sqrt(2*pi*pow(sigma,2)))*exp((-1)*(pow((yWerte[i] - ew), 2) / (2*pow(sigma, 2)))) 
        for i in range(anzSchritte)]
    
    min_dv = min(dichteVerlauf)
    dichteVerlauf = [x - min_dv for x in dichteVerlauf]
    
    max_dv = max(dichteVerlauf)
    coef = 10/ max_dv
    dichteVerlauf = [y * coef for y in dichteVerlauf]
    
    return schrittweite, dichteVerlauf

pascal = fitte_gauss (15, 2, 200)