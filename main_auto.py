import emob_only.emob_zeitreihe
import os
import numpy as np
import pandas as pd
import tkinter, tkinter.filedialog
import multiprocessing
from joblib import Parallel, delayed
from tqdm import tqdm
import re

# =============================================================================
#GUI Data Selection:
#file_path = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
root = tkinter.Tk()
folder = tkinter.filedialog.askdirectory(parent=root,title='Choose folder')

list_folder_names = []
list_filenames = []

for r, dirs, files in os.walk(folder):
    for file in files:
        if len(re.findall(r'EMob_Schluessel_.{1,4}_9.csv',file)) > 0 or len(re.findall(r'EMob_Schluessel_Zubau.{1,4}_.{1,2}.csv',file)) > 0:
            list_filenames.append(os.path.join(r, file))
            list_folder_names.append(r)
    
root.destroy()

print(list_filenames)
# =============================================================================

for i, emob_file in enumerate(tqdm(list_filenames)):
    # =============================================================================
    # config:
    nBewohner = 250000
    nWochentag = 1
    nTage = 365

    length_emob = [0] * (nTage * 24 * 4)
    emob_export = pd.Series(length_emob)
    A = pd.DataFrame()

    nPersonen = pd.read_csv(
        emob_file, 
        usecols=[1], 
        encoding='latin-1', 
        sep=';', 
        header=None
    )

    # wofür ist das?
    nPersonen = nPersonen.values.tolist()
    nPersonen = [item for sublist in nPersonen for item in sublist]

    nStandort = pd.read_csv(
        emob_file, 
        usecols=[0], 
        encoding='latin-1', 
        sep=';', 
        header=None).T
    # =============================================================================

    # =============================================================================
    # Parallelisierung:
    def generate_zeitreihe(nBewohner, nPersonen, nWochentag, nTage):
        df_ts = emob_only.emob_zeitreihe(nBewohner, nPersonen, nWochentag, nTage)
        while sum (df_ts) == 0:
            df_ts = emob_only.emob_zeitreihe(nBewohner, nPersonen, nWochentag, nTage)
            df_ts = df_ts*-1
        return df_ts

    # get cpu number
    num_cores = multiprocessing.cpu_count()
    print("Anzahl der genutzen Kerne = " + str(num_cores))

    A = Parallel(n_jobs=num_cores)(
        delayed(generate_zeitreihe)(nBewohner, nPersonen[i], nWochentag, nTage) for i in tqdm(range(len(nPersonen))))
    df = pd.concat([nStandort,pd.DataFrame(A).T.abs()], ignore_index= True)
    # =============================================================================

    # =============================================================================
    # outstream:
    # formate NNF
    new_header = df.iloc[0] #grab the first row for the header
    df = df[1:] #take the data less the header row
    df.columns = new_header #set the header row as the df header

    #a dd row with p0
    p0 = pd.DataFrame(pd.Series('p0').repeat(len(df.iloc[0]))).T
    p0.columns=df.columns
    df_nnf = pd.concat([p0,df], ignore_index = False, axis = 0)

    # add header as first row
    first_row =pd.DataFrame(new_header).T
    first_row.columns = df.columns
    df_nnf = pd.concat([first_row,df_nnf], ignore_index = False, axis = 0)

    # rename index of location and p0
    df_nnf.rename(index={0:'NNF'},inplace=True)

    # add '#'
    empty_row  = pd.DataFrame(np.nan, index=['#'],columns = df_nnf.columns)
    df_nnf = pd.concat([empty_row,df_nnf], ignore_index = False, axis = 0)

    # export
    path = list_folder_names[i]

    df_nnf.to_csv(
        path + '/EMob_Zeitreihe_nnf.csv', 
        sep=';', 
        header=False, 
        index=True,
        encoding='latin-1'
    )
    # =============================================================================
