def abwesenheit2auto(alleAbwesenheiten, wege, autos):
    
    #erstellt leere Attribute abwesenheit, verbrauch und zeitreihe für 96 1/4 pro Tag für jedes Auto
    for i, auto in enumerate(autos):
        auto.initialisiere_abwesenheiten(len(alleAbwesenheiten))
    
    #macht es nicht mehr sinn beide Objekte gemäß ihrer Startzeit zu sortieren,
    #damit zu erst die Wege abgearbeitet werden, die früher beginnen,
    #und somit eine höhere Wahrscheinlichkeit haben länger zu sein
    for tag, _ in enumerate(alleAbwesenheiten):
        abwesenheiten = alleAbwesenheiten[tag]
        tageWege = wege[tag]
        if len(abwesenheiten) > 1:
            for i,abwesend in enumerate(abwesenheiten):
                weg = tageWege[i]
                zugeteilt = False
                for auto in autos:
                    if not zugeteilt:
                        zugeteilt = auto.abwesenheit_hinzufuegen(tag, abwesend, weg)
                if not zugeteilt:
                    a = None
                    # print("Abwesenheit nicht zugeteilt!")
        elif len(abwesenheiten) == 1:
            autos[0].abwesenheit_hinzufuegen(tag, abwesenheiten[0], tageWege[0])
    
