from emob_only.haushalt import Haushalt
from emob_only.way2car import abwesenheit2auto

import sqlite3 as lite
import os

def emob_zeitreihe(nBewohner, nPersonen, nWochentag, nTage):
    ''' nWochentag: Montag=1, etc.

        Erzeugt einen Haushalt aus den übergegebenen Daten und bestimmt eine entsprechende Zeitreihe für ein Elektroauto '''
    con = lite.connect(os.getcwd() + '//emob_only//stats_auto.db')
    cur = con.cursor()
    
# =============================================================================
#     erstellt Objekt Klasse Haushalt: bestimmt Haushaltstyp, 
#     Personentyp (erstellt Objekt Klasse Person) und 
#     Anzahl an Autos und deren Typen (erstellt Objekt Klasse auto)
    
    Haus = Haushalt(cur, nBewohner, nPersonen)
# =============================================================================


    for itTag in range(nWochentag, nWochentag + nTage):
        if itTag % 7 == 0:
            tag = 7
        else:
            tag = itTag % 7
        for person in Haus.personen:
            # bestimmt für jede Person seinen Beruf und ausgehend davon erstellt Object Klasse Weg(Verkehrsmittel, Startzeit, Verweildauer, Abwesenheit)
            person.wege_hinzufuegen(tag=tag,cur=cur)

    del con, cur

    wege = {}
    abwesenheiten = {}
    for i in range(nTage):
        wege[i] = []
        abwesenheiten[i] = []
    
    #Wege und Abwesendheit entnehmen von Personen die mit dem Auto unterwegs sind    
    for person in Haus.personen:
                if person.fuehrerschein and Haus.anzahlAutos > 0:
                    for i in range(nTage):
                        #Hinweis: objekt weg enthält nochmal objekt weg (mehrere Wege pro Tag)
                        wege[i] += [weg for weg in person.wege[i] if weg.verkehrsmittel[0] == 5]
                        abwesenheiten[i] += [weg.abwesenheit for weg in person.wege[i] if weg.verkehrsmittel[0] == 5]
    #dict wege enthält nur wege von personen mit führerschein und deren wege mit Autos zurückgelegt werden sollen
    #abwesenheit analog übernommen                    
    #key = Tag
                        
    #weist den Wegen und Abwesenheit den Autos zu (gemäß chronologischem Aufruf der Personen)                  
    abwesenheit2auto(abwesenheiten, wege, Haus.autos)
    
    for auto in Haus.autos:
        auto.erzeuge_zeitreihen()
        auto.passe_zeitreihen_fortlaufend_an()

    return Haus.zeitreihe_autos()