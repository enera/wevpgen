Erzeugt bei Aufruf der Funktion emob_zeitreihe einen Haushalt mit der angegebenen Personenzahl in einem Gebiet mit der angegebenen Einwohnerzahl.
Dieser Haushalt besitzt garantiert mindestens ein Elektroauto.
Für diesen Haushalt wird anschließend eine Leistungszeitreihe in MWh, die für das Laden der Elektroautos entsteht, erzeugt.
Die Zeitreihe wird auf Basis von den Wegen, die die Mitglieder des Haushaltes mit dem Auto zurücklegen, erstellt. Hierbei wird auch der aktuelle Wochentag mit in Betracht gezogen.