class Auto(object):
    def __init__(self, cap, reichweite, verbrauch):
        self.maxSpeicher = cap/1000
        self.SpeicherStundeNull = cap/1000
        self.ladeleistung = 0.0036
        self.durchschnittsgeschwindigkeit = 32 #km/h
        self.zeitreihe = []
        self.abwesenheiten = []
        self.energieVerbrauch = verbrauch/1000 # kWh/km/1000 = MWh/km
        self.reichweite = reichweite
        self.verbrauch = []
        self.bezeichner = 'Emobilität'
        self.im_netz = True
        self.wege = []
        self.p0 = []
        self.pmin = []
        self.pmax = []
        self.qmax = []
        self.qmin = []
        self.kommentar = ''

    def entferne(self):
        self.p0 = [0]*len(self.p0)
        self.pmin = [0]*len(self.p0)
        self.pmax = [1e14]*len(self.p0)
        self.qmin = [0]*len(self.p0)
        self.qmax = [1e14]*len(self.p0)
        self.im_netz = False

    def initialisiere_abwesenheiten(self, days):
        # ein Tag hat 96 Zeitpunkte
        # 24h ^= 24*4 0.25h Schritte
        tmp = [0] * 96
        for _ in range(days):
            self.zeitreihe.append(tmp)
        self.abwesenheiten = [[] for _ in range(days)]
        self.verbrauch = [[] for _ in range(days)]

    def abwesenheit_hinzufuegen(self, day, abwesenheit, weg):
        # Weg für später speichern
        # Wenn es eine Überschneidung gibt nicht hinzufügen
        if set(abwesenheit).intersection(self.abwesenheiten[day]):
            return False
        else:
            self.wege.append(weg)
            self.abwesenheiten[day] += abwesenheit
            
            #Verbrauch in Abhängigkeit der Wegdauer bestimmen:
            #Verbrauch geringer für den Tag wenn keine Rückfahrt
            if max(abwesenheit) > 24 and day < len(self.abwesenheiten) - 1:
                self.verbrauch[day].append(
                    weg.wegdauer * self.durchschnittsgeschwindigkeit * 
                    self.energieVerbrauch
                )
            # Verbrauch der Hin- und Rückfahrt am gleichen Tag --> *2    
            else:
                self.verbrauch[day].append(
                    weg.wegdauer * self.durchschnittsgeschwindigkeit * 
                    self.energieVerbrauch #* 2 m, fuer neuen Berechungsprozess angepasst!
                )

            tmp1 = [0] * 96 
            tmp2 = [0] * 96 
            for a in abwesenheit:
                if a < 24:
                    tmp1[int(a*4)] = 1
                
                # Letzten Tag abfangen und über Tag hinausfahren handeln
                elif day < len(self.abwesenheiten) - 1:
                    tmp2[int((a-24)*4)] = 1
                    self.abwesenheiten[day+1].append(a-24)
                else:
                    continue
        
        # Falls am Tag davor über den Tag hinaus gefahren wurde das hier handeln
        # zip packt die beiden Listen zu Liste aus Tupel: 
        # (a,b), (a,b), (a,b), ...
        self.zeitreihe[day] = [1 if a==1 or b==1 else 0 for a,b in zip(self.zeitreihe[day],tmp1)]
        if day < len(self.abwesenheiten) - 1:
            self.zeitreihe[day+1] = tmp2
        
        # if True:
        #     self.__wege_sanity_check__()

        return True
    
    def verbrauch_hinzufuegen(self, tag, weg):
        self.verbrauch[tag].append(
            weg.wegdauer * self.durchschnittsgeschwindigkeit * 
            self.energieVerbrauch * 2
        )

    def erzeuge_zeitreihen(self):
        #Länge des gesamten Simulationszeitraums (warum kommentar???)
        # warum nicht kommentar = nTage*96
        self.kommentar = [0] * len(self.zeitreihe) * 96
        
        #Verbrauch den Zeitpunkten zuordnen
        for tag, z in enumerate(self.zeitreihe):
            j = 0
            for i,zeitpunkt in enumerate(z):
                # Auto ist abwesend
                if zeitpunkt == 1:
                    self.p0.append(0.0)
                    self.pmin.append(0.0)
                    self.pmax.append(0.0)
                    self.qmin.append(0.0)
                    self.qmax.append(0.0)
                # Auto ist zuhause
                else:
                    self.p0.append(0.0)
                    self.pmin.append(-self.ladeleistung)
                    self.pmax.append(self.ladeleistung)
                    self.qmin.append(-self.ladeleistung)
                    self.qmax.append(self.ladeleistung)
                
                #falls z==1 um 1 Uhr am Tag (also wenn man am Tag vorher schon unterwegs war)
                if i==0 and zeitpunkt == 1:
                    if self.verbrauch[tag-1]:
                        #Warum Verbrauch an der Stelle 0 und nicht der letzte?
                        #append in Zeile 51 fügt das letzte Element am Ende der Liste hinzu
                        #self.kommentar[i] = self.verbrauch[tag-1][0]
                        
                        #korrektur:
                        self.kommentar[i] = self.verbrauch[tag-1][-1]
                    else:
                        self.kommentar[i] = 0
                #wenn z das erste mal auftritt dann buche verbrauch        
                elif i > 0 and z[i-1] == 0 and z[i] == 1:
                    #[i*(tag+1)]????böser Fehler hier?:
                    #self.kommentar[i*(tag+1)] = self.verbrauch[tag][j]
                    self.kommentar[i+(tag*96)] = self.verbrauch[tag][j]
                    j+=1
        
        #was macht der hier?
        self.kommentar = ''.join((
            'Emobilitaet;{};{};'.format(self.maxSpeicher,self.SpeicherStundeNull),
            ';'.join([str(s) for s in self.kommentar])
        ))
    
    def zeitreihe_ist_moeglich(self):
        from gurobipy import Model, GRB

        unterwegs_zeitreihe, verbrauch_zeitreihe = self.__gib_verbrauch_zeitreihe__()
        nZeitpunkte = len(unterwegs_zeitreihe)

        m = Model("Zeitreihencheck")

        l = {} # Ladezustand des Speichers
        p_0 = {} # Leistung zum Laden
        for i in range(nZeitpunkte):
            l[i] = m.addVar(lb = 0, ub=self.maxSpeicher, name="l_{}".format(i), vtype=GRB.CONTINUOUS)
            if not unterwegs_zeitreihe[i]:
                p_0[i] = m.addVar(lb=-self.ladeleistung, ub=self.ladeleistung, name="pplus_{}".format(i), vtype=GRB.CONTINUOUS)
            elif unterwegs_zeitreihe[i]:
                p_0[i] = m.addVar(lb=0, ub=0, name="pplus_{}".format(i), vtype=GRB.CONTINUOUS)
        
        m.modelSense = GRB.MAXIMIZE
        m.setParam(GRB.Param.MIPGap, 10)
        m.setParam(GRB.Param.OutputFlag, 0)
        m.update()

        for i in range(nZeitpunkte-1):
            m.addConstr(
                l[i] + p_0[i]/4 -verbrauch_zeitreihe[i] == l[i+1]
            )
        try:
            m.addConstr(
                l[0] == self.SpeicherStundeNull
            )
        except:
            m.addConstr(
                l[0] == self.maxSpeicher/2
            )
        

        m.optimize()

        if m.status == GRB.Status.OPTIMAL:
            ladezustand = m.getAttr('x', l)
            p_0 = m.getAttr('x', p_0)

            p = [-1*p_0[i] for i in range(nZeitpunkte)]
            l = [ladezustand[i] for i in range(nZeitpunkte)]
            self.kommentar = 'Emobilitaet;{};{};'.format(self.maxSpeicher,self.SpeicherStundeNull) + ';'.join( [str(ll) for ll in ladezustand] )
            self.p0_fortlaufend = p
            return True
        
        elif m.status == GRB.Status.INFEASIBLE:
            return False

        return False

    def passe_zeitreihen_fortlaufend_an(self, test=1):
        n_weg = 0
        ladezustand = [self.SpeicherStundeNull]
        #WTF?!
        verbrauch = self.__erzeuge_verbrauch__()
        #WTF.2?! warum wird der verbrauch so oft wiederholt berechnet und warum in way2car anders?
        #WTF.3?! Verbrauch wird hier nicht den richtigen zeitreihen zugeordnet
        zeitreihe, verbrauch_zeitreihe = self.__gib_verbrauch_zeitreihe__()
        if sum(verbrauch_zeitreihe) == 0:
            return False
        elif test > 1:
            return True
        self.verbrauch_zeitreihe = verbrauch_zeitreihe
        p0 = [0]*len(verbrauch_zeitreihe)
        for i in range(len(verbrauch_zeitreihe)):
            verbrauch = verbrauch_zeitreihe[i]
            z = zeitreihe[i]
            if verbrauch != 0:
                ladezustand.append(ladezustand[-1] - verbrauch)
            elif z == 0:
                if ladezustand[-1] < self.maxSpeicher and self.ladeleistung/4 + ladezustand[-1] <= self.maxSpeicher:
                    p0[i] = -self.ladeleistung
                    ladezustand.append(self.ladeleistung/4 + ladezustand[-1])
                elif ladezustand[-1] < self.maxSpeicher:
                    p0[i] = -round(self.maxSpeicher - ladezustand[-1],6)*4
                    ladezustand.append(self.maxSpeicher)
                else:
                    ladezustand.append(ladezustand[-1])
            else:
                ladezustand.append(ladezustand[-1])
            
            if ladezustand[-1] < 0 or ladezustand[-1] > self.maxSpeicher:
                return False

        self.p0 = p0
        
        p_kum = 0
        v_kum = 0
        max_kap = self.maxSpeicher
        l0 = ladezustand[0]
        for i,p in enumerate(p0):
            p_kum += p
            v_kum += verbrauch_zeitreihe[i]
            try:
                assert(round(-l0 + p_kum/4 + v_kum,10) <= 0), "Untere Grenze verletzt!"
                assert(round(max_kap -l0 + v_kum + p_kum/4,10) >= 0), "Obere Grenze verletzt!"
            except:
                return False

        return True

        # for i,z in enumerate(zeitreihe):
        #     if z==1 and zeitreihe[i-1] == 0:
        #         delta = ladezustand[-1] - verbrauch_zeitreihe[i]/self.maxSpeicher
        #         ladezustand.append(delta)
        #     elif z==1:
        #         ladezustand.append(ladezustand[-1])
        #     else:
        #         l = ladezustand[-1]
        #         if l < 1:
        #             # kWh => kW
        #             kw_stand = l*self.maxSpeicher*4
        #             # Überlade den Speicher nicht
        #             if kw_stand + self.ladeleistung < self.maxSpeicher*4:
        #                 # Was draufgeladen wird
        #                 kwh_plus = self.ladeleistung/4
        #                 ladezustand.append(ladezustand[-1] + kwh_plus/self.maxSpeicher)
        #                 self.p0[i] = -1*self.ladeleistung
        #             else:
        #                 kwh_plus = (1-l)*self.maxSpeicher
        #                 ladezustand.append(ladezustand[-1] + kwh_plus/self.maxSpeicher)
        #                 self.p0[i] = -1*kwh_plus*4
        #         else:
        #             ladezustand.append(1.0)
        # self.l = ladezustand


        # for idx,zeitreihe in enumerate(self.zeitreihe):
        #     for jdx,_ in enumerate(zeitreihe):
        #         #Auto fährt los
        #         if zeitreihe[jdx-1]==0 and zeitreihe[jdx]==1:
        #             # Momentanen Ladezustand ermitteln
        #             delta = ladezustand[-1] - verbrauch[n_weg]/self.maxSpeicher
        #             # Falls das Auto durch die Fahrt leer ist
        #             # dann kommt er mit halb vollem Tank zurück
        #             if delta < 0:
        #                 delta = 0.5
        #             ladezustand.append(delta)
        #             n_weg += 1
        #         # Auto steht zuhause und kann geladen werden
        #         else:
        #             l = ladezustand[-1]
        #             if l < 1:
        #                 # kWh => kW
        #                 kw_stand = l*self.maxSpeicher*4
        #                 # Überlade den Speicher nicht
        #                 if kw_stand + self.ladeleistung < self.maxSpeicher*4:
        #                     # Was draufgeladen wird
        #                     kwh_plus = self.ladeleistung/4
        #                     ladezustand.append(ladezustand[-1] + kwh_plus/self.maxSpeicher)
        #                     self.p0[idx] = -1*self.ladeleistung
        #                 else:
        #                     kwh_plus = (1-l)*self.maxSpeicher
        #                     ladezustand.append(ladezustand[-1] + kwh_plus/self.maxSpeicher)
        #                     self.p0[idx] = -1*kwh_plus*4
        #             else:
        #                 ladezustand.append(1.0)
        
        return True

    def erzeuge_marktorientierte_zeitreihe(self, e_v_verhaeltnis, v_e_verhaeltnis):
        nZeitpunkte = len(e_v_verhaeltnis)
        unterwegs_zeitreihe, verbrauch_zeitreihe = self.__gib_verbrauch_zeitreihe__()
        
        from gurobipy import Model, GRB

        nZeitpunkte = len(e_v_verhaeltnis)

        m = Model("Marktorientiert")

        l = {} # Ladezustand des Speichers
        p_plus = {} # positive Wirkleistungsaenderung -> Laden
        p_minus = {} # negative Wirkleistungsaenderung -> Einspeisen
        x = {} # pplus wird benutzt
        y = {} # pminus wird benutzt
        for i in range(nZeitpunkte):
            l[i] = m.addVar(lb = 0, ub=self.maxSpeicher, name="l_{}".format(i), vtype=GRB.CONTINUOUS)
            x[i] = m.addVar(name="x_{}".format(i), vtype=GRB.BINARY)
            y[i] = m.addVar(name="y_{}".format(i), vtype=GRB.BINARY)
            if not unterwegs_zeitreihe[i]:
                p_plus[i] = m.addVar(lb=0, ub=self.ladeleistung, name="pplus_{}".format(i), obj=e_v_verhaeltnis[i], vtype=GRB.CONTINUOUS)
                p_minus[i] = m.addVar(lb=0, ub=self.ladeleistung, name="pminus_{}".format(i), obj=v_e_verhaeltnis[i], vtype=GRB.CONTINUOUS)
            elif unterwegs_zeitreihe[i]:
                p_plus[i] = m.addVar(lb=0, ub=0, name="pplus_{}".format(i), obj=e_v_verhaeltnis[i], vtype=GRB.CONTINUOUS)
                p_minus[i] = m.addVar(lb=0, ub=0, name="pminus_{}".format(i), obj=v_e_verhaeltnis[i], vtype=GRB.CONTINUOUS)
        
        m.modelSense = GRB.MAXIMIZE
        m.setParam(GRB.Param.MIPGap, 10)
        m.setParam(GRB.Param.OutputFlag, 0)
        m.update()

        for i in range(nZeitpunkte-1):
            m.addConstr(
                l[i] + p_plus[i]/4 - p_minus[i]/4 -verbrauch_zeitreihe[i] == l[i+1]
            )
        try:
            m.addConstr(
                l[0] == self.SpeicherStundeNull
            )
        except:
            m.addConstr(
                l[0] == self.maxSpeicher/2
            )
        
        # verbieten, dass pplus und pminus gleichzeitig benutzt werden
        for i in range(nZeitpunkte):
            m.addConstr(
                p_plus[i] <= self.ladeleistung*(1-y[i])
            )
            m.addConstr(
                p_plus[i] <= self.ladeleistung*(x[i])
            )
            m.addConstr(
                p_minus[i] <= self.ladeleistung*(1-x[i])
            )
            m.addConstr(
                p_minus[i] <= self.ladeleistung*(y[i])
            )

        m.optimize()

        if m.status == GRB.Status.OPTIMAL:
            ladezustand = m.getAttr('x', l)
            p_plus = m.getAttr('x', p_plus)
            p_minus = m.getAttr('x', p_minus)

            p = [p_plus[i] - p_minus[i] for i in range(nZeitpunkte)]
            l = [ladezustand[i] for i in range(nZeitpunkte)]

            
            self.kommentar = 'Emobilitaet;{};{};'.format(self.maxSpeicher,self.SpeicherStundeNull) + ';'.join( [str(ll) for ll in ladezustand] )
            # if not self.__loesung_sanity_check__(verbrauch_zeitreihe,p,l):
            #     m.write('debug/model.lp')
            #     import pickle
            #     with open("debug/emobsanity-marktorientiert.pkl",'rw+') as f:
            #         pickle.dump(f, self)
            #     raise
            
            # -1, weil hier VZS und in Integral EZS
            self.p0 = [-1*round(pp,5) for pp in p]
        
        elif m.status == GRB.Status.INFEASIBLE:
            raise
            m.computeIIS()
            m.write('inf.ilp')

        return True

    def __erzeuge_verbrauch__(self):
        verbrauch = []
        for weg in self.wege:
            verbrauch.append(
                weg.wegdauer * self.durchschnittsgeschwindigkeit * 
                self.energieVerbrauch
            )
        return verbrauch

    def __gib_verbrauch_zeitreihe__(self):
        verbrauch = self.__erzeuge_verbrauch__()
        verbrauch_zeitreihe = [0] * len(self.zeitreihe) * 96
        # if len(verbrauch_zeitreihe) > 36000:
        #     import pdb; pdb.set_trace()
        n_weg = 0
        
        # chronologische Sortierung der Verbrauchsliste über self.abwesenheiten
        #list_abwesenheiten = self.abwesenheiten
        #sort_abwesenheiten = [(x.sort(key = lambda i: i[0])) for x in list_abwesenheiten if x != []]
        zeitreihe = []
        
        for z in self.zeitreihe:
            zeitreihe += z
        
        for idx, z in enumerate(zeitreihe):
            # Auto fährt los
            if z==1 and zeitreihe[idx-1] == 0:
                if verbrauch[n_weg] <= self.maxSpeicher:
                    #Fehler: Vebrauch ist nicht chronologisch geordnet, sondern nach abwesenheiten!!!
                    verbrauch_zeitreihe[idx] = verbrauch[n_weg]
                    n_weg += 1
                else:
                    #das ist so nicht gut...
                    verbrauch_zeitreihe[idx] = self.maxSpeicher        
                
        return zeitreihe, verbrauch_zeitreihe

    def __loesung_sanity_check__(self,verbrauch_zeitreihe,p0,l):
        try:
            ladezustand = self.SpeicherStundeNull
        except:
            ladezustand = self.SpeicherStundeNull/2
        
        if min(p0) > self.ladeleistung or min(p0) < -self.ladeleistung:
            return False
        
        if round(min(l),5) < 0 or round(max(l),5) > self.maxSpeicher:
            return False

        for idx, p in enumerate(p0):
            if idx < len(p0) - 1:
                ladezustand += p/4 - verbrauch_zeitreihe[idx]
                if round(ladezustand,5) > self.maxSpeicher:
                    return False
        
        return True

    def __wege_sanity_check__(self):
        verbrauch_zeitreihe = self.__gib_verbrauch_zeitreihe__()
        anzahl_verbrauch = len([v for v in verbrauch_zeitreihe if v > 0])

        assert(anzahl_verbrauch == len(self.wege)), "Anzahl der Wege stimmt nicht mit der Anzahl an Verbrauchszeitpunkten überein"