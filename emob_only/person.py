from random import uniform
from emob_only.wege import Weg

class Person(object):
    def __init__(self, cur, haushaltstyp):
        self.haushaltstyp = haushaltstyp
        self.wege = []
        self.gruppe = self.__bestimme_personengruppe__(cur)
        self.fuehrerschein = self.__bestimme_fuehrerschein__(cur)
    
    def wege_hinzufuegen(self, cur, tag=-1):
        anzahlWege = self.__bestimme_anzahl_wege(cur)
        if tag == -1:
            #Weg in Abhängigkeit des Gruppentyps bestimmen
            self.wege.append(
                [Weg(cur, gruppe=self.gruppe) for _ in range(anzahlWege)]
            )
        else:
            #Weg in Abhängigkeit des Wochentags bestimmen
            self.wege.append(
                [Weg(cur, gruppe=self.gruppe, tag=tag) for _ in range(anzahlWege)]
            )
    
    def __bestimme_anzahl_wege(self, cur):
        cur.execute(
            "SELECT * from anzahlWege_verhaltensbezogenePersonengruppe"
        )
        rows = cur.fetchall()

        possibilites = rows[self.haushaltstyp[0]]

        return self.__throw_dice__(possibilites)

    def __bestimme_personengruppe__(self, cur):
        cur.execute(
            "SELECT * from verhaltenshomogenePersonengruppe_haushaltstyp"
        )
        rows = cur.fetchall()

        possibilites = rows[self.haushaltstyp[0]]

        gruppen = [
            'Erwerbstätige mit verfügbarem Auto',
            'Erwerbstätige ohne Auto',
            'Nichterwerbstätige mit verfügbarem Auto',
            'Nichterwerbstätige ohne Auto',
            'Studenten mit verfügbarem Auto',
            'Studenten ohne Auto',
            'Auszubildende mit verfügbarem Auto',
            'Auszubildende ohne Auto',
            'Schüler ab 11 Jahre mit verfügbarem Auto',
            'Schüler ab 11 Jahre ohne Auto'
        ]

        gruppenID = self.__throw_dice__(possibilites)

        return (gruppenID, gruppen[gruppenID])

    def __bestimme_fuehrerschein__(self, cur):
        cur.execute(
            "SELECT * from verhaltenshomogenePersonengruppe_fuehrerschein"
        )
        rows = cur.fetchall()

        possibilities = rows[self.gruppe[0]]

        if self.__throw_dice__(possibilities)  == 0:
            return 1
        else:
            return 0

    def __throw_dice__(self, possibilites):
        r = uniform(0,1)
        for i, pos in enumerate(possibilites):
            if r*100 <= pos:
                return i