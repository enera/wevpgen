from random import uniform
from emob_only.person import Person
from emob_only.auto import Auto
from random import randint
import pandas as pd
import os
# =============================================================================
# Anm.: @Dominik: 
#   - possibilites != possibilities
#   - warum sind die Funktionen nicht chronologisch angeordnet? 
# =============================================================================
class Haushalt(object):
    """ Containerklasse für den jeweiligen Haushalt """

    def __init__(self, cur, nEinwohner, nPersonen):
        
        self.personen = []
        self.autos = []
        
        typen = [
            'junge Alleinlebende',
            'Alleinlebende mittleren Alters',
            'ältere Alleinlebende',
            'junge Zweipersonenhaushalte',
            'Zweipersonenhaushalte mittleren Alters',
            'ältere Zweipersonenhaushalte',
            'Haushalte mit mind. 3 Erwachsenen',
            'Haushalte mit mind. 1 Kind unter 6 Jahren',
            'Haushalte mit mind. 1 Kind unter 14 Jahren',
            'Haushalte mit mind. 1 Kind unter 18 Jahre',
            'Alleinerziehende',
            'nicht zuzuordnen'
        ]
        
        #Haushaltstyp in Abhängigkeit der Einwohneranzahl & Personen bestimmen:
        typid = self.__haushaltstyp_bestimmen__(cur, nEinwohner, nPersonen)
        self.haushaltstyp = (typid, typen[typid])
        
        #Personenanzahl wird nicht mehr stochastisch bestimmt
        self.anzahlPersonen = nPersonen
        
        #Anzahl an Erwachsenen und Kinder bestimmen sowie Klasse Personen erstellen
        self.personen = self.__haushalt_erzeugen__(cur)
        
        #Anzahl an Autos in abhängigkeit des Haushaltstyp und unabhängig von Anzahl Erwachsener sowie Klasse Autos erstellen
        self.anzahlAutos = self.__anzahl_autos_bestimmen__(cur)

        for _ in range(self.anzahlAutos):
            self.__auto_hinzufuegen__(cur)

    def schluessel_zuweisen(self, schluessel):
        self.knoten = schluessel[0]
        self.bm_schluessel = schluessel[1]
        for auto in self.autos:
            auto.knoten = schluessel[0]
            auto.schluessel = "{}/NE-{}".format(schluessel[1], auto.bezeichner)

    def __haushalt_erzeugen__(self, cur):
        # Haushalt so erzeugen, dass er dem Typ entspricht
        nKinder = 0
        nErwachsene = 0
        minKinder, minErwachsene = self.__min_kinder_erwachsene_bestimmen__

        while nKinder < minKinder or nErwachsene < minErwachsene:
            personen = []
            for _ in range(self.anzahlPersonen):
                personen.append(Person(cur, self.haushaltstyp))
            
            nKinder, nErwachsene = __kinder_erwachsene_bestimmen__(personen)
        
        assert(len(personen) == self.anzahlPersonen)

        return personen

    @property
    def __min_kinder_erwachsene_bestimmen__(self):
        if self.haushaltstyp[0] == 0:
            return 0, 1
        elif self.haushaltstyp[0] == 1:
            return 0, 1
        elif self.haushaltstyp[0] == 2:
            return 0,1 
        elif self.haushaltstyp[0] == 3:
            return 0,2
        elif self.haushaltstyp[0] == 4:
            return 0,2
        elif self.haushaltstyp[0] == 5:
            return 0,2
        elif self.haushaltstyp[0] == 6:
            return 0,3
        elif self.haushaltstyp[0] == 7:
            return 0,2
        elif self.haushaltstyp[0] == 8:
            return 1,2
        elif self.haushaltstyp[0] == 9:
            return 1,2
        elif self.haushaltstyp[0] == 10:
            return 1,1

    def __haushaltstyp_bestimmen__(self, cur, nEinwohner, nPersonen):
# =============================================================================
#         #von Dominik:
#         cur.execute("SELECT * from haushaltstyp_gemeinde")
#         rows = cur.fetchall()
# =============================================================================
        
        rows_Einwohner = pd.read_csv(os.getcwd() + '//emob_only//data//haushaltstyp_gemeinde.csv', delimiter=';', index_col=0) 
        
        
        if nEinwohner < 2000:
            possibilites_type = rows_Einwohner.iloc[0]
        elif nEinwohner < 5000:
            possibilites_type = rows_Einwohner.iloc[1]
        elif nEinwohner < 20000:
            possibilites_type = rows_Einwohner.iloc[2]
        elif nEinwohner < 50000:
            possibilites_type = rows_Einwohner.iloc[3]
        elif nEinwohner < 100000:
            possibilites_type = rows_Einwohner.iloc[4]
        elif nEinwohner < 500000:
            possibilites_type = rows_Einwohner.iloc[5]
        elif nEinwohner > 500000:
            possibilites_type = rows_Einwohner.iloc[6]

        rows = pd.read_csv(os.getcwd() + '//emob_only//data//anzahlpersonen_haushaltstyp.csv', delimiter=';', index_col=0)
        rows = rows.drop(rows.columns[-3:], axis=1)
        
        #Liste invertieren
        rows = rows.transpose()

        df = rows*possibilites_type.values[:-1]/100
        df = df.div(df.sum(axis=1), axis=0)
        df = df.cumsum(axis=1)
        if nPersonen >=6:
            possibilites = list(df.iloc[4]*100)
        else:
            possibilites = list(df.iloc[nPersonen-1]*100)
        
        #was soll das hier??? das ist völlig sinnfrei
        ret = 13
        while ret >= 10:
            ret = self.__throw_dice__(possibilites)

        return ret
    
# =============================================================================
#       #von Dominnik:     
#       def __person_hinzufuegen__(self, cur):
#         self.personen.append(Person(cur, self.haushaltstyp))
# =============================================================================

    def __auto_hinzufuegen__(self, cur):
        cur.execute("SELECT * from haushaltstyp_autoklasse")
        rows = cur.fetchall()

        possibilites = rows[self.haushaltstyp[0]]

        autoklasse = self.__throw_dice__(possibilites)

        if autoklasse == 0:
            cur.execute("SELECT * from kleinwagen")
            autos = cur.fetchall()
        elif autoklasse == 1:
            cur.execute("SELECT * from mittelklassewagen")
            autos = cur.fetchall()
        elif autoklasse == 2:
            cur.execute("SELECT * from oberklassewagen")
            autos = cur.fetchall()
        
        _,_,cap,reichweite,verbrauch = autos[randint(0,len(autos) - 1)]

        self.autos.append(Auto(cap,reichweite,verbrauch))

# =============================================================================
#         #von Dominik:      
#         def __anzahl_personen_bestimmen(self, cur):
#         cur.execute("SELECT * from anzahlpersonen_haushaltstyp")
#         rows = cur.fetchall()
# =============================================================================
    
    def __anzahl_autos_bestimmen__(self, cur):
        # Wahrscheinlichkeiten so verändert, dass immer mindestens ein Elektroauto vorhanden ist
        rows = pd.read_csv('emob_only//data//anzahlAutos_haushaltstyp.csv', delimiter=';', index_col=0)
        rows = rows.drop(rows.columns[-3:], axis=1)
        rows = rows.drop(rows.columns[0], axis=1)

        rows = rows.div(rows.sum(axis=1), axis=0)
        rows = rows.cumsum(axis=1)
        
        possibilities = rows.iloc[self.haushaltstyp[0]].values*100

# =============================================================================
#         cur.execute("SELECT * from anzahlAutos_haushaltstyp")
#         rows = cur.fetchall()
# 
#         possibilites = rows[self.haushaltstyp[0]]
# =============================================================================
        
# =============================================================================
#         return self.__throw_dice__(possibilites) 
# =============================================================================
        
        # 1.Spalte mit 0 Autos entfernt daher + 1
        anz_A = self.__throw_dice__(possibilities) + 1
        
        if anz_A != 0:
            return anz_A
        else:
            print ("ERROR, 0 Autos")
        
# =============================================================================
#         anz_Autos = self.__throw_dice__(possibilities)
#         if anz_Autos != 0:
#             return anz_Autos
#         else:
#             return anz_Autos + 1
# =============================================================================
        
    def __throw_dice__(self, possibilites):
        r = uniform(0,1)
        for i, pos in enumerate(possibilites):
            if r*100 <= pos:
                return i

    def zeitreihe_autos(self):
        if not self.autos:
            ts = pd.Series() 
        else:   
            ts = pd.Series(0, index=range(len(self.autos[0].p0)))
            for car in self.autos:
                ts = ts.add(pd.Series(car.p0))

        return ts


def __kinder_erwachsene_bestimmen__(personen):
    kinder = 0
    erwachsene = 0
    for p in personen:
        if p.gruppe[0] <= 5:
            erwachsene += 1
        elif p.gruppe[0] <= 9:
            kinder += 1

    return kinder, erwachsene