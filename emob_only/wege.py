from random import uniform

class Weg(object):
    def __init__(self, cur, gruppe=-1, tag=-1):
        self.gruppe = gruppe
        self.tag = tag
        
        if tag == -1:
            self.zweck = self.__bestimme_zweck_gruppe__(cur)
        else:
            self.zweck = self.__bestimme_zweck_tag__(cur)

        self.verkehrsmittel = self.__bestimme_hauptverkehrsmittel__(cur)
        self.startzeit = self.__bestimme_startzeit__(cur)
        self.wegdauer = self.__bestimme_wegdauer__(cur)
        self.verweilzeit = self.__bestimme_verweilzeit__()
        self.abwesenheit = self.__bestimme_abwesenheit__()
    
    def __bestimme_abwesenheit__(self):
        #startzeit[Uhrzeit] | wegdauer = h |verweilzeit = h
        endzeit = self.startzeit[1] + self.wegdauer*2 + self.verweilzeit
        time = [self.startzeit[1]]
        i = 1
        while time[-1] < endzeit:
            time.append(time[i-1] + 0.25)
            i += 1

        # Falls die Wegdauer =0^= <0.25h dann die doppelten rausnehmen
        return sorted(list(set(time)))

    def __bestimme_verweilzeit__(self):
        # grobe Annahmen aus Studien [min]
        if self.startzeit[1] < 16:
            arbeit = 8*60
        else:
            arbeit = 4*60
        einkauf = uniform(13,21)
        freizeit = uniform(10,30)

        
        if self.zweck[0] == 0 or self.zweck[0] == 1 or self.zweck[0] == 2:
            abwesenheit = arbeit
        elif self.zweck[0] == 3 or self.zweck[0] == 4:
            abwesenheit = einkauf
        elif self.zweck[0] == 5 or self.zweck[0] == 6: 
            abwesenheit = freizeit
        
        # Verweilzeit von <5min = 0
        return round(abwesenheit/60*4)/4


    def __bestimme_wegdauer__(self, cur):
        cur.execute(
            "SELECT * from weglaenge_hauptzweck"
        )
        rows = cur.fetchall()

        possibilites = rows[self.zweck[0]]

        # Verhindern, dass ich in "weiß nciht" oder "unplausibel" würfel
        laengeID = 9
        while laengeID > 7:
            laengeID = self.__throw_dice__(possibilites)

        # Längen sind in Intervallen angegeben
        # bis 0.4, 0.4 bis 0.6, etc. [km]
        if laengeID == 0:
            raw = uniform(0,0.4)
        elif laengeID == 1:
            raw = uniform(0.4,0.6)
        elif laengeID == 2:
            raw = uniform(0.6,1)
        elif laengeID == 3:
            raw = uniform(1,2)
        elif laengeID == 4:
            raw = uniform(2,6)
        elif laengeID == 5:
            raw = uniform(6,10)
        elif laengeID == 6:
            raw = uniform(10,25)
        elif laengeID == 7:
            raw = uniform(25,100)
        
        #bestimme 1/4h Wegdauer bei Durchschnittsgeschwindigkeit von 32 km/h
        #umrechnung auf h-Werte
        #falls Weglaenge unter 5km, dann Wegdauer = 0, sinnvoll???    
        if raw == 2 or raw == 3 or raw == 4:
            raw = 5
            
        return round(raw/32*4)/4

    def __bestimme_startzeit__(self, cur):
        cur.execute(
            "SELECT * from startzeit_hauptzweck"
        )
        rows = cur.fetchall()

        possibilites = rows[self.zweck[0]]

        zeitID = 8
        while zeitID > 6:
            zeitID = self.__throw_dice__(possibilites)

        # Startzeiten sind in Intervallen angegeben
        # 5 bis 8, 8 bis 10, etc.[Uhrzeit]
        if zeitID == 0:
            raw = uniform(5,8)
        elif zeitID == 1:
            raw = uniform(8,10)
        elif zeitID == 2:
            raw = uniform(10,13)
        elif zeitID == 3:
            raw = uniform(13,16)
        elif zeitID == 4:
            raw = uniform(16,19)
        elif zeitID == 5:
            raw = uniform(19,22)
        elif zeitID == 6:
            raw = uniform(22,27)
            # Umwandlung in 24 bis 5
            if raw > 24:
                raw = raw - 24
        
        time = round(raw*4)/4

        return (zeitID, time)
    
    def __bestimme_hauptverkehrsmittel__(self, cur):
        cur.execute(
            "SELECT * from hauptverkehrsmittel_hauptzweck"
        )
        rows = cur.fetchall()

        possibilites = rows[self.zweck[0]]

        verkehrsmittel = [
            "zu Fuß",
            "Fahrrad",
            "Mofa/Moped",
            "Motorrad",
            "Auto als Mitfahrer",
            "Auto als Fahrer",
            "LKW",
            "ÖPNV",
            "Taxi",
            "Schiff, etc.",
            "anderes",
            "keine Angabe"
        ]

        mittelID = self.__throw_dice__(possibilites)

        return (mittelID, verkehrsmittel[mittelID])



    def __bestimme_zweck_gruppe__(self, cur):
        cur.execute(
            "SELECT * from hauptzweck_verhaltensbezogenePersonengruppe"
        )
        rows = cur.fetchall()

        possibilites = rows[self.gruppe[0]]

        zwecke = [
            'Arbeit',
            'dienstlich',
            'Ausbildung',
            'Einkauf',
            'Erledigung',
            'Freizeit',
            'Begleitung',
            'keine Angabe'
        ]
        zweckID = 8
        while zweckID > 6: 
            zweckID = self.__throw_dice__(possibilites)

        return (zweckID, zwecke[zweckID])

    def __bestimme_zweck_tag__(self, cur):
        cur.execute(
            "SELECT * from hauptzweck_tag"
        )
        rows = cur.fetchall()

        possibilites = rows[self.tag]

        zwecke = [
            'Arbeit',
            'dienstlich',
            'Ausbildung',
            'Einkauf',
            'Erledigung',
            'Freizeit',
            'Begleitung',
            'keine Angabe'
        ]

        zweckID = 8
        while zweckID > 6: 
            zweckID = self.__throw_dice__(possibilites)

        return (zweckID, zwecke[zweckID])

    def __throw_dice__(self, possibilites):
        r = uniform(0,1)
        for i, pos in enumerate(possibilites):
            if r*100 <= pos:
                return i