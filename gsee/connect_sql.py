import sys
import mysql.connector
import pandas as pd 
from tqdm import tqdm

def get_sql_data(voronoiID, year):   
    config = {
        'user': 'CW',
        'password': 'netzgenerator',
        'host': '134.130.175.34',
        'port': '3307',
        'charset': 'utf8'
        }
    
    db = mysql.connector.connect(**config)        
    
    #  you must create a Cursor object. It will let
    #  you execute all the queries you need
    cur = db.cursor()
    
    #check if argument year is available in database	
    data_year = [10, 11, 12]
    try:
        year in data_year
    except: 
        print ("specified year is not available")
        sys.exit(1)
        
    #typecast function arguments to strings    
    strvoronoiID = str(voronoiID)
    stryear = str(year)
#    db_data = pd.DataFrame(columns =['solar_direct','solar_global','temperature'])
    db_data = pd.DataFrame()
    
    for i in tqdm(range(1,13)):
        if i < 10:
            strmonth = "0" + str(i)
        else:
            strmonth = str(i)
            
        # define query to get solar_direct, solar_global and temperatur
        query = ("SELECT * FROM `sig_input`.`dwd_solar_direkt_%s%s_h` WHERE ID = %s" % (stryear, strmonth, strvoronoiID))
        cur.execute(query) 
        solar_direct = pd.DataFrame(cur.fetchone())
        
        query_2 = ("SELECT * FROM `sig_input`.`dwd_solar_global_%s%s_h` WHERE ID = %s" % (stryear, strmonth, strvoronoiID))
        cur.execute(query_2) 
        solar_global = pd.DataFrame(cur.fetchone())
        
        query_3 = ("SELECT * FROM `sig_input`.`dwd_temperatur_%s%s_h` WHERE ID = %s" % (stryear, strmonth, strvoronoiID))
        cur.execute(query_3) 
        solar_temperature = pd.DataFrame(cur.fetchone())
    
        #merge in one DataFrame
        df = pd.concat([solar_direct, solar_global, solar_temperature], axis = 1)
        df = df.drop([0], axis = 0)
        db_data = pd.concat([db_data, df], axis = 0, ignore_index = True)
        
        #clear iterator
        del strmonth
        del df
        
    #rename header
    db_data.columns = ["direct_horizontal", "global_horizontal","temperature"]
    db_data = db_data.apply(pd.to_numeric, downcast='float')
# =============================================================================
#     db_data['direct_fraction'] = db_data['direct_fraction'].apply(lambda x: x*0.001)
#     db_data['global_horizontal'] = db_data['global_horizontal'].apply(lambda x: x*0.001)
# =============================================================================
    
    #calculate diffuse fraction: 
    db_data['diffuse_horizontal'] = db_data.global_horizontal - db_data.direct_horizontal 
    db_data['diffuse_fraction'] = db_data.diffuse_horizontal / db_data.global_horizontal
    db_data = db_data.fillna(0)
    
    #fetch lat and long from dwd_koordinaten:    
    query_4 = ("SELECT * FROM `sig_input`.`dwd_koordinaten` WHERE ID = %s" % (strvoronoiID))
    cur.execute(query_4)
    long = cur.fetchone ()[1]
    cur.execute(query_4)
    lat = cur.fetchone()[2]
    
    #create timestamp as index of db_data
    timestamp = pd.date_range(start = '1/1/2010', end = '1/1/2011', freq = 'H', closed = 'left')
    db_data = db_data.set_index(timestamp)
    
    return(db_data, lat, long)
    db.close()
    
    