import heat_pump as hp
import matplotlib.pyplot as plt
import os
import pandas as pd
import gsee

db_data, _, _ = gsee.connect_sql.get_sql_data(240352,10)

x = hp.Waermepumpe(3,2,db_data)
x.wrapper()

# plt1 = plt.plot(x.P_WP)
# plt2 = plt.plot(x.debugTInnen)

result = pd.DataFrame(zip(x.P_WP, x.debugTInnen))
path = os.getcwd()
result.to_csv(path + '\\data_export\\WP_output.csv', sep=';')