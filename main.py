import matplotlib.pyplot as plt
import os
import pandas as pd
from tqdm import tqdm

import gsee
import emob_only.emob_zeitreihe
import heat_pump as hp

# 1.input = PLZ ID, 2.input = year
db_data, lat, lon = gsee.connect_sql.get_sql_data(240352,10)
path = os.getcwd()

# =============================================================================
# Heat-Pump Model:
hp_output = hp.Waermepumpe(3,2,db_data)
hp_output.wrapper()
# 
plt1 = plt.plot(hp_output.P_WP)
plt2 = plt.plot(hp_output.debugTInnen)

hp_export = pd.DataFrame(zip(hp_output.P_WP, hp_output.debugTInnen))
hp_export.to_csv(path + '\\data_export\\WP_output.csv', sep=';')
# =============================================================================


# =============================================================================
# PV-Power Simulation Model:
pv_export = gsee.pv.run_model(
    db_data,
    coords=(lat, lon),     # Latitude and longitude
    tilt=30,                # 30 degrees tilt angle
    azim=180,               # facing towards equator
    tracking=0,             # fixed - no tracking
    capacity=1000,          # 1000W
)

print(lat,lon)
pv_export.to_csv(path + '\\data_export\\PV_output.csv', sep=';')
# =============================================================================


# =============================================================================
# E-Mobility Simulation Model:
nBewohner = 25000
#nPersonen = 5
nWochentag = 1
nTage = 7

length_emob = [0]* (nTage*24*4)
emob_export = pd.Series(length_emob)
A = pd.DataFrame()
nPersonen = [1, 1, 1, 2, 2, 3, 3, 3, 4, 4, 5, 5, 7]

# =============================================================================
# #verschiedene Zeitreihen erstellen:
# for i in tqdm(range(12)):
#     df_ts = emob_only.emob_zeitreihe(nBewohner, nPersonen[i], nWochentag, nTage)
#     A = pd.concat([A,df_ts], axis = 1, sort = False) 
# emob_export = A
# =============================================================================

for i in tqdm(range(200)):
    df_ts = emob_only.emob_zeitreihe(nBewohner, 5, nWochentag, nTage)
    emob_export = emob_export.add(df_ts)

emob_export.to_csv(path + '\\data_export\\auto_export.csv', sep = ';')

