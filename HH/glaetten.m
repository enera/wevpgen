function dblMatOutput = glaetten(dblMatInput, Filter, intVerlaengerung)

% glaetten Glaettet die Zeitreihe mit dem angegeben Filter
%
% Syntax:
%   dblMatOutput = glaetten(dblMatInput, Filter, intVerlaengerung)
%
% Inputs:
%   dblMatInput         Zeitreihe im Format (Wert;Wert; ...)
%   Filter              Glaettungsfilter oder ein Name (s.u.)
%   intVerlaengerung    Optional: Um Anfang bzw. Ende besser glaetten zu
%                       koennen, werden zusätzliche Werte in die
%                       Eingangzeitreihe eingefügt.
%
% Outputs:
%   dblMatOutput        Die geglaettete(unverschobene!) Zeitreihe
%
% See also: conv2

if ischar(Filter)
    strFilter = Filter;
    
    if strcmpi(strFilter, 'Spencer')
        % 15-Punkte Formel von Spencer (Zeitreihen, Seite 112)
        dblVecFilter = [-3, -6, -5, 3, 21, 46, 67, 74, 67, 46, 21, 3, -5, -6, -3]' / 320;
        
    elseif strcmpi(strFilter(1), 'B')
        % Binomialfilter
        intGrad = str2double(strFilter(2:end));
        f = @(x) arrayfun(@(y) nchoosek(x, y), (0:x)');
        dblVecFilter = f(intGrad);
        dblVecFilter = dblVecFilter / sum(dblVecFilter);
        
    elseif strcmpi(strFilter(1), 'G')
        % Gaussfilter
        dblSigma = str2double(strFilter(2:end));
        dblSize = 6 * dblSigma + 1;
        if dblSize > 0
            dblVecX = linspace(-dblSize / 2, dblSize / 2, dblSize)';
            dblVecFilter = exp(-dblVecX .^ 2 / (2 * dblSigma ^ 2));
            dblVecFilter = dblVecFilter / sum(dblVecFilter);
        else
            dblVecFilter = 1;
        end
    
    else
        % Gleitender Durchschnitt (Zeitreihen, Seite 114)
        
        if strcmpi(strFilter, 'M0L3')
            % Polynomgrad 0 + 1 (gleitender Durchschnitt)
            % 3 Werte
            dblVecFilter = [1, 1, 1]' / 3;
        elseif strcmpi(strFilter, 'M0L5')
            % Polynomgrad 0 + 1 (gleitender Durchschnitt)
            % 5 Werte
            dblVecFilter = [1, 1, 1, 1, 1]' / 5;
        elseif strcmpi(strFilter, 'M0L7')
            % Polynomgrad 0 + 1 (gleitender Durchschnitt)
            % 7 Werte
            dblVecFilter = [1, 1, 1, 1, 1, 1, 1]' / 7;
        elseif strcmpi(strFilter, 'M2L5');
            % Polynomgrad 2 + 3
            % 5 Stützstellen
            dblVecFilter = [-3, 12, 17, 12, -3]' / 35;
        elseif strcmpi(strFilter, 'M2L7');
            % Polynomgrad 2 + 3
            % 7 Stützstellen
            dblVecFilter = [-2, 3, 6, 7, 6, 3, -2]' / 21;
        elseif strcmpi(strFilter, 'M2L9');
            % Polynomgrad 2 + 3
            % 9 Stützstellen
            dblVecFilter = [-21, 14, 39, 54, 59, 54, 39, 14, -21]' / 231;
        end
    end
else
    dblVecFilter = Filter(:);
end

if nargin == 2
    intVerlaengerung = 0;
end

if intVerlaengerung > 0
    dblMatInput = [dblMatInput(1 : intVerlaengerung, :); dblMatInput; dblMatInput((end - intVerlaengerung + 1) : end, :)];
elseif intVerlaengerung < 0
    dblMatInput = [dblMatInput((end - (-intVerlaengerung) + 1) : end, :); dblMatInput; dblMatInput(1 : (-intVerlaengerung), :)];
end

dblMatOutput = conv2(dblMatInput, dblVecFilter);

% Die künstlich erzeugen Werte und Verschiebung entfernen
intVecPos = (floor(length(dblVecFilter)/2+1) : (size(dblMatInput, 1) + floor(length(dblVecFilter)/2)));

dblMatOutput = dblMatOutput(intVecPos, :);

if abs(intVerlaengerung) > 0
    dblMatOutput = dblMatOutput(abs(intVerlaengerung) + 1 : (end - abs(intVerlaengerung)), :);
end

