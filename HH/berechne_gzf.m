function [ gzf ] = berechne_gzf(matrix)


anzahl_zeitreihen = size(matrix,2);
summe_beider_max = 0;
summe_zeitreihen = zeros(length(matrix),1);

for ii = 1 : anzahl_zeitreihen
    summe_beider_max = summe_beider_max + max(matrix(:,ii));
    summe_zeitreihen = summe_zeitreihen + matrix(:,ii);
end
min_summe = min(summe_zeitreihen);
max_summe = max(summe_zeitreihen);
gzf_max = max_summe / summe_beider_max;
gzf_min = min_summe / summe_beider_max;
gzf(1,1) = gzf_max;
gzf(1,2) = gzf_min;

end