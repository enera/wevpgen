function [ dblVecVerbrauch, dblVecErzeugung ] = verbinde_Verbrauch_Erzeugung( dblVecVerbrauch, dblMatErzeugung, dblSpeicherkap, dblSpeicherLeistung)


n = length(dblVecVerbrauch);
n2 = length(dblMatErzeugung);

dblWirkungsgrad = 1;
dblLadeleistung = dblSpeicherLeistung; 
dblEntladeleistung = dblSpeicherLeistung; 

if n ~= n2
    error('L�nge von Verbraucher Zeitreihe(dblVecVerbrauch) und Erzeuger(dblMatrixErzeugung) m�ssen identisch sein')
else
    
    dblMatZeitreihen = zeros(n,2);
    
    %erm�glicht mehrere Erzeuger, z.B. PV-Anlage und BHKW
    dblVecErzeugung = sum(dblMatErzeugung,2);
    
    if dblSpeicherkap < 0
        error('Gr��e des Speichers(dblSpeicher) muss gr��er/gleich Null sein')
    elseif dblSpeicherkap == 0
        dblMatZeitreihen(:,1) = dblVecVerbrauch - dblVecErzeugung;
        binNeg = dblMatZeitreihen(:,1) < 0;
        dblMatZeitreihen(binNeg,2) = -dblMatZeitreihen(binNeg,1);
        dblMatZeitreihen(binNeg,1) = 0;
    else
        dblSpeicherstand = 0;
        for ii = 1 : n
            dbldiff = dblVecVerbrauch(ii) - dblVecErzeugung(ii);
            
            %weniger Erzeugung als Verbrauch
            if dbldiff > 0
                
                %Erzeugung wird auf 0 gesetzt
                
                dblMatZeitreihen(ii,2) = 0;
                if dblSpeicherstand > 0
                    
                    %Entladeleistung/4 gr��er als Inhalt?
                    
                    helpmin = min(dblEntladeleistung/4, dblSpeicherstand);
                    
                    %Maximal der notwendige Verbrauch wird entladen
                    
                    dblSpeicherleerung = min(helpmin * dblWirkungsgrad, (dbldiff/4));
                    
                    %Entladene Energie wird Speicher entnommen
                    
                    dblSpeicherstand = dblSpeicherstand - dblSpeicherleerung;
                    
                    %Verbrauch wird um Erzeugungsleistung und Entladeleistung korrigiert 
                    
                    dblMatZeitreihen(ii,1) = dbldiff - (dblWirkungsgrad * dblSpeicherleerung*4) ;
                else
                    dblMatZeitreihen(ii,1) = dbldiff;
                end
            %mehr Erzeugung als Verbrauch
            else
                
                %Verbrauch wird komplett aus Einspeisung gedeckt
                
                dblMatZeitreihen(ii,1) = 0;
                
                %Einspeisung wird gesenkt um Betrag, der f�r Verbrauch
                %genutzt wird
                
                dblMatZeitreihen(ii,2) = dblVecErzeugung(ii) - dblVecVerbrauch(ii);
                
                %Resteinspeisung wird ggf. in Speicher verteilt
                
                if dblSpeicherstand < dblSpeicherkap
                    
                    %Ladeleistung/4 gr��er als Restkapazit�t?
                    
                    helpmin = min(dblLadeleistung/4,dblSpeicherkap-dblSpeicherstand);
                    
                    %Maximale noch vorhandene Erzeugung wird gespeichert,
                    %oder m�gliche Restkapazit�t
                    
                    dblSpeicherfuellung = min(helpmin ,  dblMatZeitreihen(ii,2)* dblWirkungsgrad/4);
                    
                    %Gespeicherte Energie wird in Speicher hinzugef�gt
                    
                    dblSpeicherstand = dblSpeicherstand + dblSpeicherfuellung;
                    
                    %Zeitreihe wird um gespeicherte Leistung reduziert
                    
                    dblMatZeitreihen(ii,2) = dblMatZeitreihen(ii,2) - (dblSpeicherfuellung*4)/dblWirkungsgrad;
                else
                    dblMatZeitreihen(ii,2) = -dbldiff;
                end
            end
        end
    end
end
dblVecVerbrauch = dblMatZeitreihen(:,1);
dblVecErzeugung = dblMatZeitreihen(:,2);
end
