cellVecKorrelationsMatrix_Industrie2 = cell(1,6);
for aa = 1 : size(dblMatMuster_Industrie,2)
    dblMatEpsilon=bsxfun(@minus,cellVecIndustrieReal{1,aa},dblMatMuster_Industrie(:,aa));
    
    korrelations_matrix_Industrie2 = cell(1,size(dblMatEpsilon,2));
    dblMatKorrelation = cell(1,size(dblMatEpsilon,2));
    for jj = 1 : size(dblMatEpsilon,2)
        dblMatVerschoben = dblMatEpsilon(:,jj);
        for ii = size(dblMatEpsilon,1) : -1 : size(dblMatEpsilon,1)-94
            dblMatVerschoben(:,end+1) = [dblMatEpsilon(ii:35040,jj);dblMatEpsilon(1:ii-1,jj)];
        end
        
        for ll = 1 :96
            for tt = 1 : 96
                dblMatKor = corrcoef([dblMatVerschoben(:,ll),dblMatVerschoben(:,tt)]);
                dblMatKorrelation{1,jj}(ll,tt) = myround(dblMatKor(1,2),4);
            end
        end
        korrelations_matrix_Industrie2{1,jj} = nearcorr(dblMatKorrelation{1,jj},1E-13,[],[],[],[],1);
    end
    
    dblMatErstezeileMatrix = zeros(size(dblMatEpsilon,2),96);
    for ii = 1 : size(korrelations_matrix_Industrie2,2)
        dblMatErstezeileMatrix(ii,:) = korrelations_matrix_Industrie2{1, ii}(1,:);
        %     plot(korrelations_matrix_Industrie2{1, ii}(1,:))
        %     hold on
    end
    
%     [dblMatErstezeileMatrixFilter, intVecAusreisser, strFehler] = myZeitreihenAusreisser(dblMatErstezeileMatrix');
%     dblMatErstezeileMatrixFilter = dblMatErstezeileMatrixFilter';
%     
%     korrelations_matrix_Industrie2 = [];
%     for ii = 1 : size(korrelations_matrix_Industrie2,2)
%         if ~ismember(ii,intVecAusreisser)
%             korrelations_matrix_Industrie2{1,end+1} = korrelations_matrix_Industrie2{1,ii};
%         end
%     end
    
    cellVecKorrelationsMatrix_Industrie2{1,aa} = korrelations_matrix_Industrie2;
    clear korrelations_matrix_Industrie2
end