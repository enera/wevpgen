function [dblVecZeitreiheNeu] = korregiereMaxLeistung(dblVecIndustrie, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster, dblVecZeitreihe)
dblProzent = 0.9;
[dblVecMaxIndustrie,indexMax] = sort(dblVecZeitreihe);
dblMaxLeistungQuotient = dblVecIndustrie(1,3)/dblVecMaxIndustrie(end);
count = 0;
while dblMaxLeistungQuotient > 1.5 || dblMaxLeistungQuotient < 0.7
    count = count + 1;
    dblVecZeitreihe = erstelleZeitreihe(false, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster);
    dblVecZeitreihe = dblVecZeitreihe / sum(dblVecZeitreihe);
    dblVecZeitreihe = dblVecZeitreihe * dblVecIndustrie(1,2) * 4;
    
    [dblVecMaxIndustrie,indexMax] = sort(dblVecZeitreihe);
    dblMaxLeistungQuotient = dblVecIndustrie(1,3)/dblVecMaxIndustrie(end);
    if count == 1
        dblMaxLeistungQuotient_prev = dblMaxLeistungQuotient;
        dblVecZeitreihe_prev = dblVecZeitreihe;
    end
    
    if dblMaxLeistungQuotient > 1.5 && count > 1
        if dblMaxLeistungQuotient < dblMaxLeistungQuotient_prev
            dblMaxLeistungQuotient_prev = dblMaxLeistungQuotient;
            dblVecZeitreihe_prev = dblVecZeitreihe;
        end
    elseif dblMaxLeistungQuotient < 0.7 && count > 1
        if dblMaxLeistungQuotient > dblMaxLeistungQuotient_prev
            dblMaxLeistungQuotient_prev = dblMaxLeistungQuotient;
            dblVecZeitreihe_prev = dblVecZeitreihe;
        end
    end
    if count == 10
        dblMaxLeistungQuotient = dblMaxLeistungQuotient_prev;
        dblVecZeitreihe = dblVecZeitreihe_prev;
        [dblVecMaxIndustrie,indexMax] = sort(dblVecZeitreihe);
        break
    end
end

if dblMaxLeistungQuotient > 1
    temp = dblVecMaxIndustrie > 0;
    firstpos = find(temp,1,'first');
    binVerringern(1:35040*dblProzent) = true;
    binVerringern(35040*dblProzent+1:35040) = false;
    binErhoehen = ~binVerringern;
    binVerringern(1:firstpos-1) = false;
    
    dblVecZeitreiheNeu(indexMax(binErhoehen),1) = dblVecZeitreihe(indexMax(binErhoehen)) * dblMaxLeistungQuotient;
    dblVecZeitreiheNeu(indexMax(binVerringern),1) = dblVecZeitreihe(indexMax(binVerringern));
    dblDiff = sum(dblVecZeitreiheNeu)-sum(dblVecZeitreihe);
    for ii = 1 : 1000000
        if dblDiff < 0.00001
            break
        elseif dblDiff > 0.00001 && ii == 1000000
            error('Profil konnte nicht angepasst werden')
        else
            dblDiffSplit = dblDiff/sum(binVerringern == 1);
            index2 = find(dblVecMaxIndustrie>0,1,'first');
            index1 = max(index2 - 1,0);
            if index1 > 0
                minDiff_erlaubt = dblVecMaxIndustrie(index2)-dblVecMaxIndustrie(index1);
            else
                minDiff_erlaubt = dblVecMaxIndustrie(index2);
            end
            binPos = dblVecMaxIndustrie > 0;
            dblVecMaxIndustrie(binPos) = bsxfun(@minus, dblVecMaxIndustrie(binPos), minDiff_erlaubt);
            
            if dblDiffSplit > minDiff_erlaubt
                dblDiffSplit = minDiff_erlaubt;
                dblVecZeitreiheNeu(indexMax(binVerringern),1) = bsxfun(@minus,dblVecZeitreiheNeu(indexMax(binVerringern)),dblDiffSplit);
                dblDiff = sum(dblVecZeitreiheNeu)-sum(dblVecZeitreihe);
                binVerringern(1:find(dblVecMaxIndustrie > 0 == 1,1,'first')) = false;
            else
                dblVecZeitreiheNeu(indexMax(binVerringern),1) = bsxfun(@minus,dblVecZeitreiheNeu(indexMax(binVerringern)),dblDiffSplit);
                dblDiff = sum(dblVecZeitreiheNeu)-sum(dblVecZeitreihe);
            end
        end
    end
    
elseif  dblMaxLeistungQuotient < 1
    %     disp('passe Leistung nach unten an')
    dblVecZeitreiheNeu = dblVecZeitreihe * dblMaxLeistungQuotient;
    intGrenzwert = mean(dblVecZeitreiheNeu);
    dblDiff = sum(dblVecZeitreihe)-sum(dblVecZeitreiheNeu);
    
    for jj = 1 : 1000000
        if dblDiff < 0.00001
            break
        else
            intAnzahl = sum(dblVecZeitreiheNeu < intGrenzwert);
            dblDiffSplit = min(dblDiff/intAnzahl,intGrenzwert-max(dblVecZeitreiheNeu(dblVecZeitreiheNeu < intGrenzwert)));
            dblVecZeitreiheNeu(dblVecZeitreiheNeu < intGrenzwert,1) = bsxfun(@plus,dblVecZeitreiheNeu(dblVecZeitreiheNeu < intGrenzwert,1),dblDiffSplit);
            dblDiff = sum(dblVecZeitreihe)-sum(dblVecZeitreiheNeu);
        end
    end
end
% dblMatKorr = corrcoef(dblVecZeitreihe,dblVecZeitreiheNeu);
% disp(dblMatKorr(1,2))
end