function [dblMatNNFgefiltert] = filtereNNF(dblMatZeitreihenErstellt,intAnzahlErzeugung,intAnzahlGes,intAnzahlAnlage)
    dblMatErzeugung = dblMatZeitreihenErstellt(:,1:intAnzahlErzeugung);
    dblVecErzeugung = sum(dblMatErzeugung,2);
    [~,indexErzeugung] = sort(dblVecErzeugung);
    
    dblMatLast = dblMatZeitreihenErstellt(:,intAnzahlErzeugung + 1 : end);
    dblVecLast = sum(dblMatLast,2);
    [~,indexLast] = sort(dblVecLast);
    
    indexNNF = indexErzeugung(end-intAnzahlGes:end);
    indexNNF = [indexNNF;indexLast(end-intAnzahlGes:end)];
    
    for ii = 1 : size(dblMatZeitreihenErstellt,2)
        [~,index] = sort(dblMatZeitreihenErstellt(:,ii));
        indexNNF = [indexNNF;index(end-intAnzahlAnlage:end)];
    end
    indexNNF = unique(indexNNF);
    dblMatNNFgefiltert = dblMatZeitreihenErstellt(indexNNF,:);
end