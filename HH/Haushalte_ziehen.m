function [ dblMatHaushalteErstellt ] = Haushalte_ziehen( dblMatHaushalte )
%HAUSHALTE_ZIEHEN Da in den untersuchten Netzen aggregierte Lasten f�r
%Haushalte erstellt werden, werden diese gesondert hier gezogen

    load('Haushalte.mat');          %Haushalte laden
    dblMatHaushalteErstellt = zeros(length(Haushalte(:,1)),length(dblMatHaushalte(:,1)));
    for ii = 1:length(dblMatHaushalte(:,1))
        energiemenge_soll = dblMatHaushalte(ii,1);      %Energiemenge der Haushalte
        energiemenge_ist = 0;                           %Gezogene Energiemenge
        
        %Es werden solange neue Haushalte erstellt, bis Energiemenge
        %erreicht
        
        while(energiemenge_ist < energiemenge_soll)
            gezogener_Haushalt = randi(length(Haushalte(1,:)));
            
            %Zeitreihen werden aufsummiert
            
            dblMatHaushalteErstellt(:,ii) = dblMatHaushalteErstellt(:,ii) + Haushalte(:,gezogener_Haushalt) / 1000;
            energiemenge_ist = energiemenge_ist + sum(Haushalte(:,gezogener_Haushalt))/4;
        end
    end
    

end

