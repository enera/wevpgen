function [ dblVecFehlerICDF_Fixed ] = fix_dblMatFehlerICDF( dblVecFehlerICDF, dblVecEpsilon )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
dblVecFehlerICDF_Fixed = dblVecFehlerICDF;
binZuKlein = dblVecFehlerICDF < min(dblVecEpsilon);
dblVecFehlerICDF_Fixed(1,binZuKlein) = min(dblVecEpsilon);
binZuGross = dblVecFehlerICDF > max(dblVecEpsilon);
dblVecFehlerICDF_Fixed(1,binZuGross) = max(dblVecEpsilon);

end

