function [cellVecSchluessel,dblMatZeitreihenErstellt,dblMatZeitreihenGefiltert] = generiereZeitreihen(strEingang,binFilter)
if nargin < 2
    binFilter = false; % ob NNF gefiltert werden sollen, oder alle 35040 genutzt werden
    intAnzahlGes = 0; % Menge der gr��ten NNF-F�lle in Anbetracht der Gesamtlast
    intAnzahlAnlage = 0; % Menge der gr��ten NNF-F�lle in Anbetracht der Gesamtlast
    dblMatZeitreihenGefiltert = [];
else
    intAnzahlGes = 500; % Menge der gr��ten NNF-F�lle in Anbetracht der Gesamtlast
    intAnzahlAnlage = 100; % Menge der gr��ten NNF-F�lle in Anbetracht der Einzelquerzweige
end
dateiname = cell2mat(regexp(strEingang,'\_\w*','match'));
[ dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie, cellVecSchluessel_Wind, cellVecSchluessel_PV, cellVecSchluessel_HH, cellVecSchluessel_Industrie ] = einlesenCSV(strEingang);
% load('Ausgangsdaten.mat')
%[dblMatWindErstellt, dblMatPVErstellt, dblMatHaushalteErstellt, cellVecIndustrieErstellt] = erstelleAlleZeitreihen(dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie);
[dblMatWindErstellt, dblMatPVErstellt, dblMatHaushalteErstellt, cellVecIndustrieErstellt] = erstelleAlleZeitreihenPar(dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie);

%% Daten zusammenfassen
cellVecSchluessel = [cellVecSchluessel_Wind, cellVecSchluessel_PV, cellVecSchluessel_HH, cellVecSchluessel_Industrie];

dblMatZeitreihenErstellt = zeros(35040,size(cellVecSchluessel_Wind,2) + size(cellVecSchluessel_PV,2) + size(cellVecSchluessel_HH,2) + size(cellVecSchluessel_Industrie,2));

if ~isempty(dblMatWindErstellt)
    dblMatZeitreihenErstellt(:,1:size(dblMatWindErstellt,2)) = dblMatWindErstellt;
end

if ~isempty(dblMatPVErstellt)
    indexStart = size(dblMatWindErstellt,2) + 1;
    indexEnde = indexStart + size(dblMatPVErstellt,2) - 1;
    dblMatZeitreihenErstellt(:,indexStart:indexEnde) = dblMatPVErstellt;
end

if ~isempty(dblMatHaushalteErstellt)
    indexStart = size(dblMatWindErstellt,2) + size(dblMatPVErstellt,2) + 1;
    indexEnde = indexStart + size(dblMatHaushalteErstellt,2) - 1;
    dblMatZeitreihenErstellt(:,indexStart:indexEnde) = dblMatHaushalteErstellt;
end

indexStart = size(cellVecSchluessel_Wind,2) + size(cellVecSchluessel_PV,2) + size(dblMatHaushalteErstellt,2) + 1;
for ii = 1 : size(cellVecIndustrieErstellt,2)
    indexEnde = indexStart + size(cellVecIndustrieErstellt{1,ii},2) - 1;
    dblMatZeitreihenErstellt(:,indexStart:indexEnde) = cellVecIndustrieErstellt{1,ii};
    indexStart = indexEnde + 1;
end

if binFilter
    intAnzahlErzeugung = size(dblMatWindErstellt,2) + size(dblMatPVErstellt,2);
    dblMatZeitreihenGefiltert = filtereNNF(dblMatZeitreihenErstellt,intAnzahlErzeugung,intAnzahlGes,intAnzahlAnlage);
end

%% Daten in XLS Speichern
cellVecBuffer = cell(1,size(cellVecSchluessel,2));
cellVecBuffer(1,1:end) = cellstr('p0');
xlswrite(['erstellte_nnf' dateiname],cellVecSchluessel,1,'A1')
xlswrite(['erstellte_nnf' dateiname],cellVecBuffer,1,'A2')
xlswrite(['erstellte_nnf' dateiname],dblMatZeitreihenErstellt,1,'A3')



if binFilter
    xlswrite(['erstellte_gefilterte_nnf' dateiname],cellVecSchluessel,1,'A1')
    xlswrite(['erstellte_gefilterte_nnf' dateiname],cellVecBuffer,1,'A2')
    xlswrite(['erstellte_gefilterte_nnf' dateiname],dblMatZeitreihenErstellt,1,'A3')
end

end
