function [ dblVecZeitreiheNeu ] = generiereIndustrie( dblVecIndustrie, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster, cellVecKorrelationsmatrix_WP, dblMatFehlerICDF_WP, dblVecMuster_WP)

%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%dblQuotient = dblLeistung / dblEnergieverbrauch;

dblVecZeitreihe = erstelleZeitreihe(false, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster);
dblVecZeitreihe = dblVecZeitreihe / sum(dblVecZeitreihe);

if dblVecIndustrie(1,2) > 0 % Energiemenge angegeben
    dblVecZeitreihe = dblVecZeitreihe / sum(dblVecZeitreihe);
    dblVecZeitreihe = dblVecZeitreihe * dblVecIndustrie(1,2) * 4;
    if dblVecIndustrie(1,3) > 0 % Energiemenge angegeben und Leistungswert angegeben
        dblVecZeitreiheNeu = korregiereMaxLeistung(dblVecIndustrie, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster, dblVecZeitreihe);
    else
        dblVecZeitreiheNeu = dblVecZeitreihe;
    end
else % nur Leistungswert angegeben
    dblVecZeitreihe = dblVecZeitreihe / max(dblVecZeitreihe);
    dblVecZeitreiheNeu = dblVecZeitreihe * dblVecIndustrie(1,3);   
end
if dblVecIndustrie(1,7) > 0 % Wärmepumpen vorhanden
    dblVecWP = erstelleZeitreiheWP( cellVecKorrelationsmatrix_WP, dblMatFehlerICDF_WP, dblVecMuster_WP);
    dblVecWP = dblVecWP * dblVecIndustrie(1,7)/max(dblVecWP);
    
    dblVecZeitreiheNeu = dblVecZeitreiheNeu + dblVecWP;
end
end