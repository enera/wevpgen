function [ minKorr_Erstellt, meanKorr_Erstellt, maxKorr_Erstellt, GZFMax_Erstellt, GZFMin_Erstellt, GZFMax_Real, GZFMin_Real, GZFGesamt_Erstellt, GZFGesamt_Real, dblMatAutokorrErstellt, dblMatAutokorrReal ] = erstelleBewertungKomplett(dblMatZeitreihenHH,cellVecIndustrieErstellt)
dblMatZeitreihenAuswahlGes = [];
dblMatZeitreihenGes = [];
load('dblMatHaushalteReal.mat')
load('cellVecIndustrieReal.mat')
load('dblMatKorrelationenReal.mat')

if ~isempty(dblMatZeitreihenHH)
    index{1} = 1 : size(dblMatZeitreihenHH,2);
end

intSpalte = randi(size(dblMatHaushalteReal,2),1,size(dblMatZeitreihenHH,2));
dblMatZeitreihenAuswahl = dblMatHaushalteReal(:,intSpalte);
dblMatZeitreihenAuswahlGes = [dblMatZeitreihenAuswahlGes,dblMatZeitreihenAuswahl];
dblMatZeitreihenGes = [dblMatZeitreihenGes,dblMatZeitreihenHH];
for ii = 1 : 6
    if isempty(cellVecIndustrieErstellt{1,ii})
        continue
    end
    dblMatDaten = cellVecIndustrieReal{1,ii};
    intSpalte = randi(size(dblMatDaten,2),1,size(cellVecIndustrieErstellt{1,ii},2));
    dblMatZeitreihenAuswahl = dblMatDaten(:,intSpalte);
    dblMatZeitreihenAuswahlGes = [dblMatZeitreihenAuswahlGes,dblMatZeitreihenAuswahl];
    dblMatZeitreihenGes = [dblMatZeitreihenGes,cellVecIndustrieErstellt{1,ii}];
    if isempty(dblMatZeitreihenHH) && ii == 1
        intstart = 1;
    elseif isempty(dblMatZeitreihenHH) && ii > 1
        intstart = index{ii-1}(1,end)+1;
    else
        intstart = index{ii}(1,end)+1;
    end
    intende = intstart + size(cellVecIndustrieErstellt{1,ii},2) - 1;
    if ~isempty(dblMatZeitreihenHH)
        index{1+ii} = intstart : intende;
    else
        index{ii} = intstart : intende;
    end
end

dblMatKorrGes = corrcoef(dblMatZeitreihenGes);
GZFGesamt_Erstellt = berechne_gzf(dblMatZeitreihenGes);
GZFGesamt_Real = berechne_gzf(dblMatZeitreihenAuswahlGes);
for ii = 1 : size(index,2)
    dblMatAutokorrTemp = [];
    dblMatAutokorrTemp2 = [];
    for kk = index{ii}(1,1) : index{ii}(1,end)
        dblMatAutokorrTemp(:,end+1)  = autocorr(dblMatZeitreihenGes(:,kk));
        dblMatAutokorrTemp2(:,end+1)  = autocorr(dblMatZeitreihenAuswahlGes(:,kk));
    end
    dblMatAutokorrErstellt(:,ii) = mean(dblMatAutokorrTemp(:,~isnan(dblMatAutokorrTemp(1,:))),2);
    dblMatAutokorrReal(:,ii) = mean(dblMatAutokorrTemp2(:,~isnan(dblMatAutokorrTemp2(1,:))),2);
    for jj = 1 : size(index,2)
        
        
        
        
        dblMatTemp = dblMatKorrGes(index{ii},index{jj});
        minKorr_Erstellt(ii,jj)=min(dblMatTemp(dblMatTemp < 1));
        meanKorr_Erstellt(ii,jj)=mean(dblMatTemp(dblMatTemp < 1));
        maxKorr_Erstellt(ii,jj)=max(dblMatTemp(dblMatTemp < 1));
        
        
        dblVecGZF = berechne_gzf([dblMatZeitreihenGes(:,index{ii}),dblMatZeitreihenGes(:,index{jj})]);
        GZFMax_Erstellt(ii,jj) = dblVecGZF(1,1);
        GZFMin_Erstellt(ii,jj) = dblVecGZF(1,2);
        
        dblVecGZF = berechne_gzf([dblMatZeitreihenAuswahlGes(:,index{ii}),dblMatZeitreihenAuswahlGes(:,index{jj})]);
        GZFMax_Real(ii,jj) = dblVecGZF(1,1);
        GZFMin_Real(ii,jj) = dblVecGZF(1,2);
    end
end

end