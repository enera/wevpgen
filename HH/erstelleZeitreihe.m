function [ dblVecZeitreihe ] = erstelleZeitreihe(binHH, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster)


if ~binHH
    dblVecZiehung = copularnd('Gaussian',korrelationsmatrix,1);
end

dblVecFehlerRnd= zeros(35040,1);
count = 0;
for tt = 1 : 365
    if  binHH
        dblVecZiehung = copularnd('Gaussian',korrelationsmatrix,1);
    end
    
    for ii= 1 : 96
        count = count + 1;
        intSpalte=round(dblVecZiehung(:)*100);
        intSpalte(intSpalte==0) = 1;
        
        dblVecFehlerRnd(count,1) = dblMatFehlerICDF(count,intSpalte(ii));
    end
end


dblVecZeitreihe = dblVecMuster + dblVecFehlerRnd;


end

