function [ dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie, cellVecSchluessel_Wind, cellVecSchluessel_PV, cellVecSchluessel_HH, cellVecSchluessel_Industrie ] = einlesenCSV( strName )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%TestCSV = importfile(strName, 2, inf);
TestCSV = importfile2(strName, 2, inf);



cellVecNamen = {'Haushalt','produzierendes Gewerbe','Business Base','Business Samstag','Einzelhandel','Gastronomie','Business Peak'};

dblMatHaushalte = [];
dblMatIndustrie = [];
dblMatPV = [];
dblMatWind = [];
cellVecSchluessel_Wind = [];
cellVecSchluessel_PV = [];
cellVecSchluessel_HH = [];
cellVecSchluessel_Industrie = [];
for ii = 1 : size(TestCSV,1)
    if ~isempty(strfind(TestCSV{ii,2},'Wind'))
        cellVecSchluessel_Wind{end+1} = TestCSV{ii,1};
        dblMatWind(end+1,1) = TestCSV{ii,4}; % Leistung
        dblMatWind(end,2) = TestCSV{ii,9}; % X Koordinate
        dblMatWind(end,3) = TestCSV{ii,10}; % Y Koordinate
    
    elseif ~isempty(strfind(TestCSV{ii,2},'PV'))
        cellVecSchluessel_PV{end+1} = TestCSV{ii,1};
        dblMatPV(end+1,1) = TestCSV{ii,4}; % Leistung
        dblMatPV(end,2) = TestCSV{ii,9}; % X Koordinate
        dblMatPV(end,3) = TestCSV{ii,10}; % Y Koordinate
        dblMatPV(end,4) = 0; % Zugehörigkeit Verbrauchertyp
        dblMatPV(end,5) = 0; % Index von Verbraucher im Typ
        for nn = 1 : size(cellVecNamen,2)
            if ~isempty(strfind(TestCSV{ii,2},cellVecNamen{1,nn}))
                dblMatPV(end,4) = nn; % Zugehörigkeit Verbrauchertyp
                dblMatPV(end,5) = str2double(regexp(TestCSV{ii,2},['\d+\.?\d*'],'match')); % Index von Verbraucher im Typ
                break
            end
        end
        if  dblMatPV(end,4) == 0 && ~isempty(strfind(TestCSV{ii,2},'('))
            error('Bezeichner von zugehöriger Verbraucherzeitreihe ist falsch. Bitte überprüfen')
        end
        
    elseif strcmp(TestCSV{ii,2},'Haushalt')
        cellVecSchluessel_HH{end+1} = TestCSV{ii,1};
        dblMatHaushalte(end+1,1) = TestCSV{ii,3}; %Energiemenge
        dblMatHaushalte(end,2) = TestCSV{ii,5}; %Speichergröße
        dblMatHaushalte(end,3) = TestCSV{ii,6}; %Speicherleistung
        dblMatHaushalte(end,4) = TestCSV{ii,7}; %Leistung Emob
        dblMatHaushalte(end,5) = TestCSV{ii,8}; %Leistung Wärmepumpe
    elseif strcmp(TestCSV{ii,2},'produzierendes Gewerbe')%schwarz
        cellVecSchluessel_Industrie{end+1} = TestCSV{ii,1};
        dblMatIndustrie(end+1,1) = 1; %Cluster
        dblMatIndustrie(end,2) = TestCSV{ii,3}; %Energiemenge
        dblMatIndustrie(end,3) = TestCSV{ii,4}; %Leistung
        dblMatIndustrie(end,4) = TestCSV{ii,5}; %Speichergröße
        dblMatIndustrie(end,5) = TestCSV{ii,6}; %Speicherleistung
        dblMatIndustrie(end,6) = TestCSV{ii,7}; %Leistung Emob
        dblMatIndustrie(end,7) = TestCSV{ii,8}; %Leistung Wärmepumpe
    elseif strcmp(TestCSV{ii,2},'Business Base')%magenta
        cellVecSchluessel_Industrie{end+1} = TestCSV{ii,1};
        dblMatIndustrie(end+1,1) = 2;
        dblMatIndustrie(end,2) = TestCSV{ii,3}; %Energiemenge
        dblMatIndustrie(end,3) = TestCSV{ii,4}; %Leistung
        dblMatIndustrie(end,4) = TestCSV{ii,5}; %Speichergröße
        dblMatIndustrie(end,5) = TestCSV{ii,6}; %Speicherleistung
        dblMatIndustrie(end,6) = TestCSV{ii,7}; %Leistung Emob
        dblMatIndustrie(end,7) = TestCSV{ii,8}; %Leistung Wärmepumpe
    elseif strcmp(TestCSV{ii,2},'BusinessSamstag')%cyan
        cellVecSchluessel_Industrie{end+1} = TestCSV{ii,1};
        dblMatIndustrie(end+1,1) = 3;
        dblMatIndustrie(end,2) = TestCSV{ii,3}; %Energiemenge
        dblMatIndustrie(end,3) = TestCSV{ii,4}; %Leistung
        dblMatIndustrie(end,4) = TestCSV{ii,5}; %Speichergröße
        dblMatIndustrie(end,5) = TestCSV{ii,6}; %Speicherleistung
        dblMatIndustrie(end,6) = TestCSV{ii,7}; %Leistung Emob
        dblMatIndustrie(end,7) = TestCSV{ii,8}; %Leistung Wärmepumpe
    elseif strcmp(TestCSV{ii,2},'Einzelhandel')%rot
        cellVecSchluessel_Industrie{end+1} = TestCSV{ii,1};
        dblMatIndustrie(end+1,1) = 4;
        dblMatIndustrie(end,2) = TestCSV{ii,3}; %Energiemenge
        dblMatIndustrie(end,3) = TestCSV{ii,4}; %Leistung
        dblMatIndustrie(end,4) = TestCSV{ii,5}; %Speichergröße
        dblMatIndustrie(end,5) = TestCSV{ii,6}; %Speicherleistung
        dblMatIndustrie(end,6) = TestCSV{ii,7}; %Leistung Emob
        dblMatIndustrie(end,7) = TestCSV{ii,8}; %Leistung Wärmepumpe
    elseif strcmp(TestCSV{ii,2},'Gastronomie')%grün
        cellVecSchluessel_Industrie{end+1} = TestCSV{ii,1};
        dblMatIndustrie(end+1,1) = 5;
        dblMatIndustrie(end,2) = TestCSV{ii,3}; %Energiemenge
        dblMatIndustrie(end,3) = TestCSV{ii,4}; %Leistung
        dblMatIndustrie(end,4) = TestCSV{ii,5}; %Speichergröße
        dblMatIndustrie(end,5) = TestCSV{ii,6}; %Speicherleistung
        dblMatIndustrie(end,6) = TestCSV{ii,7}; %Leistung Emob
        dblMatIndustrie(end,7) = TestCSV{ii,8}; %Leistung Wärmepumpe
    elseif strcmp(TestCSV{ii,2},'BusinessPeak')%blau
        cellVecSchluessel_Industrie{end+1} = TestCSV{ii,1};
        dblMatIndustrie(end+1,1) = 6;
        dblMatIndustrie(end,2) = TestCSV{ii,3}; %Energiemenge
        dblMatIndustrie(end,3) = TestCSV{ii,4}; %Leistung
        dblMatIndustrie(end,4) = TestCSV{ii,5}; %Speichergröße
        dblMatIndustrie(end,5) = TestCSV{ii,6}; %Speicherleistung
        dblMatIndustrie(end,6) = TestCSV{ii,7}; %Leistung Emob
        dblMatIndustrie(end,7) = TestCSV{ii,8}; %Leistung Wärmepumpe
    end
    
end

end

