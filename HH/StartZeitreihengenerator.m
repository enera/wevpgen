function StartZeitreihengenerator()

addpath(genpath('Zeitreihenmodell EE'));

[FileName,PathName] = uigetfile({'*.dat;*.csv','Data Files (*.dat,*.csv)';'*.*','All Files (*.*)'},'Pick Another File','/home/');

[ dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie, cellVecSchluessel_Wind, cellVecSchluessel_PV, cellVecSchluessel_HH, cellVecSchluessel_Industrie ] = einlesenCSV(FileName);

dblMatHaushalteErstellt = [];
% if ~isempty(dblMatHaushalte)
%     dblMatHaushalteErstellt = Haushalte_ziehen( dblMatHaushalte );        %Haushalte werden gezogen
%     dblMatHaushalte = [];
% end

% load('Ausgangsdaten.mat')
%[dblMatWindErstellt, dblMatPVErstellt, dblMatHaushalteErstellt, cellVecIndustrieErstellt] = erstelleAlleZeitreihen(dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie);
[dblMatWindErstellt, dblMatPVErstellt, dblMatHaushalteErstellt, cellVecIndustrieErstellt] = erstelleAlleZeitreihenPar(dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie);

%%Weil es im Zeitreihenmodell keine 0 gibt werden die untersten 2% auf 0
%%gesetzt (aus realen Messwerten abgeleitet)

if ~isempty(dblMatWindErstellt)
    fprintf('4Prozent der Windzeitreihen werden pauschal heuristisch auf 0 korrigiert.\n');
    for ii = 1:length(dblMatWindErstellt(1,:))
        sort_zeitreihe = sort(dblMatWindErstellt(:,ii));
        max_zweiprozent = sort_zeitreihe(round(length(sort_zeitreihe)*0.04)+1);
        dblMatWindErstellt(find(dblMatWindErstellt(:,ii) < max_zweiprozent),ii) = 0;
    end
end

if ~isempty(dblMatIndustrie)
    [~,index]=sort(dblMatIndustrie(:,1));
    cellVecSchluessel_Industrie_sortiert = cellVecSchluessel_Industrie(index);
else
    cellVecSchluessel_Industrie_sortiert = cellVecSchluessel_Industrie;
end
cellVecSchluessel = [cellVecSchluessel_Wind, cellVecSchluessel_PV, cellVecSchluessel_HH, cellVecSchluessel_Industrie_sortiert];

dblMatZeitreihenErstellt = zeros(35040,size(cellVecSchluessel_Wind,2) + size(cellVecSchluessel_PV,2) + size(cellVecSchluessel_HH,2) + size(cellVecSchluessel_Industrie,2));

if ~isempty(dblMatWindErstellt)
    dblMatZeitreihenErstellt(:,1:size(dblMatWindErstellt,2)) = dblMatWindErstellt;
end

if ~isempty(dblMatPVErstellt)
    indexStart = size(dblMatWindErstellt,2) + 1;
    indexEnde = indexStart + size(dblMatPVErstellt,2) - 1;
    dblMatZeitreihenErstellt(:,indexStart:indexEnde) = dblMatPVErstellt;
end

if ~isempty(dblMatHaushalteErstellt)
    indexStart = size(dblMatWindErstellt,2) + size(dblMatPVErstellt,2) + 1;
    indexEnde = indexStart + size(dblMatHaushalteErstellt,2) - 1;
    dblMatZeitreihenErstellt(:,indexStart:indexEnde) = dblMatHaushalteErstellt;
end

indexStart = size(cellVecSchluessel_Wind,2) + size(cellVecSchluessel_PV,2) + size(dblMatHaushalteErstellt,2) + 1;
for ii = 1 : size(cellVecIndustrieErstellt,2)
    indexEnde = indexStart + size(cellVecIndustrieErstellt{1,ii},2) - 1;
    dblMatZeitreihenErstellt(:,indexStart:indexEnde) = cellVecIndustrieErstellt{1,ii};
    indexStart = indexEnde + 1;
end

cellVecBuffer = cell(1,size(cellVecSchluessel,2));
cellVecBuffer(1,1:end) = cellstr('p0');

%% Die erstellten Zeitreihen werden durchsucht, sodass Ersatzlasten wieder
%aggregiert werden

%Zun�chst alle Ersatzlasten finden

% Nummern_Vektor = [];
% for ii = 1:length(cellVecSchluessel)
%     Nummer = 0;
%     if ~isempty(findstr(cellVecSchluessel{ii},'Ersatzlast'))
%         Nummer = cellVecSchluessel{ii}(end-4:end-2);
%         
%         %Da Bezeichung in Form 512, _15 oder t_1 auftauchen kann: Nummer
%         %korrigieren
%         
%         if Nummer(1) == '_' | Nummer(1) == 't' 
%             Nummer = Nummer(2:3);
%         end
%         if Nummer(1) == '_' | Nummer(1) == 't' 
%             Nummer = Nummer(2);
%         end
%         Nummer = str2num(Nummer);
%     end
%     Nummern_Vektor(ii,1) = Nummer;
% end

%Alle gefundenen doppelten Nummern zusammenfassen

% zu_loeschen = [];
% for ii = 1:max(Nummern_Vektor)
%     Ersatzlasten = find(Nummern_Vektor(:,1) == ii);
%     if ~isempty(Ersatzlasten)
%         for jj = 2:length(Ersatzlasten)
%             dblMatZeitreihenErstellt(:,Ersatzlasten(1)) = dblMatZeitreihenErstellt(:,Ersatzlasten(1)) + dblMatZeitreihenErstellt(:,Ersatzlasten(jj));
% 
%             Zusammengefasste Lasten speichern, um sp�ter zu l�schen
% 
%             zu_loeschen(1,end+1) = Ersatzlasten(jj);
%         end
% 
%         Name der aggregierten Last korrigieren
% 
%         finde_unterstriche = findstr(cellVecSchluessel{Ersatzlasten(1)},'_');
%         cellVecSchluessel(Ersatzlasten(1)) = {cellVecSchluessel{Ersatzlasten(1)}(1:finde_unterstriche(3)-1)};
%     end
% end

% % Ersatzlasten, die aggregiert wurden, l�schen
% 
% cellVecSchluessel(zu_loeschen) = [];
% cellVecBuffer(zu_loeschen) = [];
% dblMatZeitreihenErstellt(:,zu_loeschen) = [];

%von 35040 auf 8760 NNF reduzieren

% for ii = 1:8760
%     zu_mergende_werte = dblMatZeitreihenErstellt((ii-1)*4+1:ii*4,:);
%     %dblMatZeitreihenErstellt(ii,:) = sum(dblMatZeitreihenErstellt((ii-1)*4+1:ii*4,:))/4;
%     dblMatZeitreihenErstellt(ii,:) = zu_mergende_werte(randi(4),:);
% end
% dblMatZeitreihenErstellt(8761:end,:) = [];

%% Achtung: zu Testzwecken werden NNF auf 200 reduziert.
 
%   fprintf('Achtung: NNF werden reduziert auf 180 (Testzwecke)\n.');
%   dblMatZeitreihenErstellt(181:end,:) = [];

nnf = [cellVecSchluessel;cellVecBuffer;num2cell(dblMatZeitreihenErstellt)];
for ii = 1:length(dblMatZeitreihenErstellt(:,1))
    nnf_zahl(ii+2,1) = ii;
end
nnf = [num2cell(nnf_zahl),nnf];
nnf{1,1} = [];
nnf{2,1} = [];

dblMatZeitreihenErstelltIndex = [];
for ii = 1:length(dblMatZeitreihenErstellt(:,1))
    dblMatZeitreihenErstelltIndex(ii,1) = ii;
end

nnf_header = [];
for ii = 1:10
    nnf_header{ii,1} = '#';
end
nnf_header{11,1} = 'NNF';
nnf_header{12,1} = 'NNF';
nnf_header(11,2:length(cellVecSchluessel)+1) = cellVecSchluessel;
nnf_header(12,:) = {'p0'};

%cellMatOutput = ['NNF', cellVecSchluessel;'NNF', cellVecBuffer];
cell2csv([PathName FileName(1:end-4) '_nnf.csv'], nnf_header,';')
dlmwrite([PathName FileName(1:end-4) '_nnf.csv'], [dblMatZeitreihenErstelltIndex dblMatZeitreihenErstellt],'-append', 'delimiter', ';','roffset',1);
%xlswrite([strEingang(1:end-10) 'Daten_nnf.xlsx'],nnf,'nnf');

% xlswrite(['erstellte_nnf_' strEingang],cellVecSchluessel,1,'A1')
% xlswrite(['erstellte_nnf_' strEingang],cellVecBuffer,1,'A2')
% xlswrite(['erstellte_nnf_' strEingang],dblMatZeitreihenErstellt,1,'A3')