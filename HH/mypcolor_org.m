function mypcolor_org(dblMatPlot, dblVecDaten, intWPT, dblVecQuantile, dblVecMax)

if nargin == 1
    dblVecDaten = [];
end

% I. A. bestimmt das 0.99%-Quantil die Farbachse (symmetrisch um 0)
if nargin < 4
    dblVecQuantile = 0.99;
    dblVecMax = [];
elseif nargin < 5
    dblVecMax = [];
end

% Eingabe ggf. umformatieren
if size(dblMatPlot, 2) == 1 && nargin >= 3
    dblMatPlot = reshape(dblMatPlot, intWPT, [])';
    if ~isempty(dblVecDaten)
        dblVecDaten = dblVecDaten(1 : intWPT : end);
    end
end


intWPT = size(dblMatPlot, 2);

% 7 Werte deutet auf Tage hin
if intWPT == 7
    intVecPosX = 1 : 7;
else
    intVecPosX = 1 : (intWPT / 4) : (intWPT + 1);
end

% Uhrzeiten nur angeben, wenn sei zum Format passen
if ismember(intWPT, [24, 96, 24*60, 24*60*60])
    dblVecV = (0 : intWPT) / intWPT;
else
    dblVecV = [];
end

% Die Matrix vergr��ern, damit alles angezeigt wird
dblMatPlot(:, end + 1) = 0;
dblMatPlot(end + 1, :) = 0;

% Die Orientierunt der Achse �ndern und kein Gitter anzeigen
pcolor(dblMatPlot);
set(gca, 'YDir', 'reverse');
shading flat

if ~isempty(dblVecDaten)
    if length(dblVecDaten) > 6
        intVecPosY = floor(linspace(1, length(dblVecDaten), 6)) + 0.5;
    else
        intVecPosY = floor(linspace(1, length(dblVecDaten), length(dblVecDaten))) + 0.5;
    end
    set(gca, 'YTick', intVecPosY);
    set(gca, 'YTickLabel',  datestr(dblVecDaten(intVecPosY - 0.5), 'dd.mm.yyyy'));
end

% Hierdurch steht der Strich mittig unter dem Wert
set(gca, 'XTick', intVecPosX + 0.5);
set(gca, 'TickDir', 'out');

if ~isempty(dblVecV)
    set(gca, 'XTickLabel', datestr(dblVecV(intVecPosX), 'HH:MM'));
else
    set(gca, 'XTickLabel', cellfun(@num2str, num2cell(intVecPosX), 'Uniformoutput', false));
end

colorbar
colormap(jet)
%Farbachse wie gew�nscht skalieren
if length(dblVecQuantile) == 1
    dblC = quantile(abs(dblMatPlot(:)), dblVecQuantile);
    caxis([-dblC, dblC]);
elseif length(dblVecQuantile) == 2
    caxis(quantile(dblMatPlot(:), dblVecQuantile));
elseif length(dblVecMax) == 1
    dblC = max(abs(dblVecMax));
    caxis([-dblC, dblC]);
elseif length(dblVecMax) == 2
    caxis(dblVecMax);
end
