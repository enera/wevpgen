function multi_GGQP_AusNNF(path)
%addpath('U:\Matlab\')
addpath('X:\Angolini (ma)\Code Version 1307')
cd(path)
structerzeugungsdaten = dir('*Erzeugung*.csv');
structausgangsdaten = dir('*Eingang*.csv');
structnetz = dir('*netz*');
cellVecSchluesselEE = [];
dblMatZeitreihenErstelltEE = [];
if ~isempty(structerzeugungsdaten)
    [cellVecSchluesselEE,dblMatZeitreihenErstelltEE] = generiereZeitreihen(structerzeugungsdaten.name);
end
dblMatAutokorr = zeros(97,size(structausgangsdaten,1));
dblMatFaktoren = zeros(size(structausgangsdaten,1),9);
cellVecNamen = cell(size(structausgangsdaten,1),1);
for ii = 1 : size(structausgangsdaten,1)
    dateiname = cell2mat(regexp(structausgangsdaten(ii).name,'\_\w*','match'));
    dateiname = strrep(dateiname,'_','');
    [cellVecSchluessel,dblMatZeitreihenErstellt] = generiereZeitreihen(structausgangsdaten(ii).name);
%     dblMatAutokorrTemp = zeros(97,size(dblMatZeitreihenErstellt,2));
%     for jj = 1 : size(dblMatZeitreihenErstellt,2)
%         dblMatAutokorrTemp(:,jj) = autocorr(dblMatZeitreihenErstellt(:,jj));
%     end
%     dblMatAutokorr(:,ii) = mean(dblMatAutokorrTemp(:,~isnan(dblMatAutokorrTemp(1,:))),2);
%     dblMatFaktoren(ii,1:2) = berechne_gzf(dblMatZeitreihenErstelltEE);
%     dblMatFaktoren(ii,3:4) = berechne_gzf(dblMatZeitreihenErstellt);
%     dblMatKorr = corrcoef(dblMatZeitreihenErstelltEE);
%     dblMatFaktoren(ii,7)=mean(dblMatKorr(dblMatKorr < 1));
%     dblMatKorr = corrcoef(dblMatZeitreihenErstellt);
%     dblMatFaktoren(ii,8)=mean(dblMatKorr(dblMatKorr < 1));
    
    cellVecSchluesselGes = [cellVecSchluesselEE cellVecSchluessel];
    dblMatZeitreihenErstelltGes = [dblMatZeitreihenErstelltEE dblMatZeitreihenErstellt];
%     dblMatFaktoren(ii,5:6) = berechne_gzf([sum(dblMatZeitreihenErstelltEE,2) sum(dblMatZeitreihenErstellt,2)]);
%     dblMatKorr = corrcoef(dblMatZeitreihenErstelltGes);
%     dblMatTemp = dblMatKorr(1:size(dblMatZeitreihenErstelltEE,2),size(dblMatZeitreihenErstelltEE,2)+1:end);
%     dblMatFaktoren(ii,9)=mean(dblMatTemp(~isnan(dblMatTemp)));
    
    %dblMatZeitreihenGefiltertGes = filtereNNF(dblMatZeitreihenErstelltGes,size(dblMatZeitreihenErstelltEE,2),intAnzahlGes,intAnzahlAnlage);
    relevanteNNF = GGQP_AusNNF2(structnetz.name,cellVecSchluesselGes,dblMatZeitreihenErstelltGes,dateiname);
    %GGQP_AusNNF(structnetz.name,cellVecSchluesselGes,dblMatZeitreihenGefiltertGes,[dateiname '_gefiltert']);
%     dblMatFaktoren(ii,10:11)=relevanteNNF(:,1)';
%     dblMatFaktoren(ii,12:13)=relevanteNNF(:,2)';
    cellVecNamen{ii,1} = dateiname;
end
% cellVecKriterien = {'max_GZF Einspeisung','min_GZF Einspeisung','max_GZF Last','min_GZF Last','max_GZF Gesamt','min_GZF Gesamt','durchsch. Korrelation Einspeisung','durchsch. Korrelation Last','durchsch. Korrelation Gesamt',...
%     'Last NNF 1','Einspeisung NNF 1','Last NNF 2','Einspeisung NNF 2'};
% % cellMatFaktoren = [cellVecNamen mat2cell(dblMatFaktoren)];
% xlswrite('Berwertungskriterien.xls', cellVecKriterien ,1,'A1');
% xlswrite('Berwertungskriterien.xls', cellVecNamen ,1,'A2');
% xlswrite('Berwertungskriterien.xls', dblMatFaktoren ,1,'B2');


end