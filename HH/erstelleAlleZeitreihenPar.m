function [dblMatErzeugungWindErstellt, dblMatErzeugungPVErstellt, dblMatHaushaltErstellt,cellVecIndustrieErstellt] = erstelleAlleZeitreihenPar(dblMatWind, dblMatPV, dblMatHaushalte, dblMatIndustrie)

binGeladen = false;
%% Laden der Ausgangsdaten
if size(dblMatHaushalte,1) > 0
    cellKorrelationsMatrix_HH = load('cellKorrelationsMatrix_HH.mat');
    cellKorrelationsMatrix_HH = cellKorrelationsMatrix_HH.cellKorrelationsMatrix_HH;  
    cellFehlerICDF_HH = load('cellFehlerICDF_HH.mat');
    cellFehlerICDF_HH = cellFehlerICDF_HH.cellFehlerICDF_HH;
    cellMuster_HH = load('cellMuster_HH.mat');
    cellMuster_HH = cellMuster_HH.cellMuster_HH;
    cellMuster_Emob = load('cellMuster_Emob.mat');
    cellMuster_Emob = cellMuster_Emob.cellMuster_Emob;
    cellFehlerICDF_Emob = load('cellFehlerICDF_Emob.mat');
    cellFehlerICDF_Emob = cellFehlerICDF_Emob.cellFehlerICDF_Emob;
    cellMuster_Emob = load('cellMuster_Emob.mat');
    cellMuster_Emob = cellMuster_Emob.cellMuster_Emob;
    
    if sum(dblMatHaushalte(:,5)) > 0
        cellFehlerICDF_WP = load('cellFehlerICDF_WP.mat');
        cellMuster_WP = load('cellMuster_WP.mat');
        cellKorrelationsmatrix_WP = load('cellKorrelationsmatrix_WP.mat');
        binGeladen = true;
    else % da Variablen als Parameter �bergeben werden
        cellFehlerICDF_WP = cell(1);
        cellMuster_WP = cell(1);
        cellKorrelationsMatrix_WP = cell(1);
    end
end

if size(dblMatIndustrie,1) > 0
    cellVecKorrelationsmatrix_Industrie = load('cellVecKorrelationsmatrix_Industrie2.mat');
    cellVecKorrelationsmatrix_Industrie = cellVecKorrelationsmatrix_Industrie.cellVecKorrelationsMatrix_Industrie;
    dblMatMuster_Industrie = load('dblMatMuster_Industrie_neu2.mat');
    dblMatMuster_Industrie = dblMatMuster_Industrie.dblMatMuster_Industrie;
    cellVecFehlerICDF_Industrie = load('cellVecFehlerICDF_Industrie_neu2.mat');
    cellVecFehlerICDF_Industrie = cellVecFehlerICDF_Industrie.cellVecFehlerICDF_Industrie;
    if sum(dblMatIndustrie(:,5)) > 0 && ~binGeladen
        cellFehlerICDF_WP = load('cellFehlerICDF_WP.mat');
        cellMuster_WP = load('cellMuster_WP.mat');
        cellKorrelationsmatrix_WP = load('cellKorrelationsmatrix_WP.mat');
    elseif ~binGeladen % da Variablen als Parameter �bergeben werden
        cellFehlerICDF_WP = cell(1);
        cellMuster_WP = cell(1);
        cellKorrelationsMatrix_WP = cell(1);
    end
end

%% Erstellen von Wind und PV Erzeugung
disp('Start Erstellung EE-Zeitreihen')
dblMatErzeugungWindErstellt = [];
dblMatErzeugungPVErstellt = [];
if ~isempty(dblMatPV) || ~isempty(dblMatWind)
    dblMatErzeugungEEErstellt = Generiere_Zeitreihe_EE(dblMatPV, dblMatWind);
    dblMatErzeugungEEErstellt = dblMatErzeugungEEErstellt(1:35040,:);
    if ~isempty(dblMatWind)
        dblMatErzeugungWindErstellt = dblMatErzeugungEEErstellt(:,1:size(dblMatWind,1));
        dblMatErzeugungWindErstellt = bsxfun(@rdivide, dblMatErzeugungWindErstellt, max(dblMatErzeugungWindErstellt));
        dblMatErzeugungWindErstellt = bsxfun(@times, dblMatErzeugungWindErstellt, dblMatWind(:,1)');
    end
    if ~isempty(dblMatPV)
        dblMatErzeugungPVErstellt = dblMatErzeugungEEErstellt(:,1+size(dblMatWind,1):end);
        dblMatErzeugungPVErstellt = bsxfun(@rdivide, dblMatErzeugungPVErstellt, max(dblMatErzeugungPVErstellt));
        dblMatErzeugungPVErstellt = bsxfun(@times, dblMatErzeugungPVErstellt, dblMatPV(:,1)');
    end
end
disp([num2str(size(dblMatWind,1)) ' Wind-Zeitreihen und '   num2str(size(dblMatPV,1)) ' PV-Zeitreihen erstellt'])

%% Erstellen von Haushalten

disp('Start Erstellung Haushalte')

dblMatHaushaltErstellt = zeros(35040,size(dblMatHaushalte,1));
count = 0;
if ~isempty(dblMatHaushalte)
    parfor hh = 1 : size(dblMatHaushalte,1)
        inthelp = 1;
        %Pr�fe ob Haushalt Emob besitzt
        if dblMatHaushalte(hh,4) > 0
            dblMatHaushaltErstellt(:,hh) = generiereHaushalt(dblMatHaushalte(hh,:), cellKorrelationsMatrix_HH{inthelp}, cellFehlerICDF_Emob{inthelp}, cellMuster_Emob{inthelp},...
                cellKorrelationsMatrix_WP{inthelp}, cellFehlerICDF_WP{inthelp}, cellMuster_WP{inthelp});
        else
            dblMatHaushaltErstellt(:,hh) = generiereHaushalt(dblMatHaushalte(hh,:), cellKorrelationsMatrix_HH{inthelp}, cellFehlerICDF_HH{inthelp}, cellMuster_HH{inthelp},...
                cellKorrelationsMatrix_WP{inthelp}, cellFehlerICDF_WP{inthelp}, cellMuster_WP{inthelp});
            %             if dblMatHaushalte(hh,5) > 0
            %
            %                 dblMatWP(:,hh) = erstelleZeitreiheWP(cellKorrelationsMatrix_WP{inthelp}, cellFehlerICDF_WP{inthelp}, cellMuster_WP{inthelp});
            %                 dblMatWP(:,hh) = dblMatWP(:,hh) * dblMatHaushalte(hh,5)/max(dblMatWP(:,hh));
            %
            %                 dblMatHaushaltErstellt(:,hh) = dblMatHaushaltErstellt(:,hh) + dblMatWP(:,hh);
            %             end
        end
    end
    %Pr�fe ob Haushalt noch eine PV-Anlage hat
    for hh = 1 : size(dblMatHaushalte,1)
        if ~isempty(dblMatPV)
            intSpalte = findSpalte(dblMatPV, hh);
            if ~isempty(intSpalte)
                dblMatErzeugung = dblMatErzeugungPVErstellt(:,intSpalte);
                [dblMatHaushaltErstellt(:,hh), dblMatErzeugungPVErstellt(:,intSpalte)] = verbinde_Verbrauch_Erzeugung(dblMatHaushaltErstellt(:,hh), dblMatErzeugung, dblMatHaushalte(hh,2), dblMatHaushalte(hh,3));
                count = count + 1;
            end
        end
    end
end
disp([num2str(size(dblMatHaushalte,1)) ' Haushalts-Zeitreihen erstellt und ' num2str(count) ' PV-Zeitreihen angepasst'])

%% Erstellen von Gewerbe/Industrie
disp('Start Erstellung Gewerbe')
intVecClusterCounter = zeros(1,6);
cellVecIndustrieErstellt = cell(1,6);
count = 0;
if ~isempty(dblMatIndustrie)
    parfor ii = 1 : size(dblMatIndustrie,1)
        intCluster = dblMatIndustrie(ii,1);
        inthelp = 1;
        cellVecKorrelationsMatrix = cellVecKorrelationsmatrix_Industrie{1,intCluster};
        dblMatFehlerICDF = cellVecFehlerICDF_Industrie{1,intCluster};
        dblVecMuster = dblMatMuster_Industrie(:,intCluster);
        
        dblMatIndustrieErstellt(:,ii) = generiereIndustrie(dblMatIndustrie(ii,:), cellVecKorrelationsMatrix, dblMatFehlerICDF, dblVecMuster,...
            cellKorrelationsMatrix_WP{inthelp}, cellFehlerICDF_WP{inthelp}, cellMuster_WP{inthelp});
    end
    
    for ii = 1 : size(dblMatIndustrie,1)
        intCluster = dblMatIndustrie(ii,1);
        intVecClusterCounter(1,intCluster) = intVecClusterCounter(1,intCluster) + 1;
        cellVecIndustrieErstellt{1,intCluster}(:,end+1) = dblMatIndustrieErstellt(:,ii);
        %Pr�fe ob Verbraucher noch eine PV-Anlage hat
        if ~isempty(dblMatPV)
            intSpalte = find(dblMatPV(:,4) == 1 + intCluster & dblMatPV(:,5) == intVecClusterCounter(1,intCluster));
            if ~isempty(intSpalte)
                dblMatErzeugung = dblMatErzeugungPVErstellt(:,intSpalte);
                [cellVecIndustrieErstellt{1,intCluster}(:,end), dblMatErzeugungPVErstellt(:,intSpalte)] = verbinde_Verbrauch_Erzeugung(cellVecIndustrieErstellt{1,intCluster}(:,end), dblMatErzeugung, dblMatIndustrie(ii,4), dblMatIndustrie(ii,5));
                count = count + 1;
            end
        end
    end
end
disp([num2str(size(dblMatIndustrie,1)) ' Gewerbe_Zeitreihen erstellt und ' num2str(count) ' PV-Zeitreihen angepasst'])
end
