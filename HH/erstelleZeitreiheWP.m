function [ dblVecZeitreihe ] = erstelleZeitreiheWP( cellVecKorrelationsmatrix, dblMatFehlerICDF, dblVecMuster_WP)

dblVecFehlerRnd= zeros(12000,1);
count = 0;
for tt = 1 : 365
    if  tt <= 125 || tt >= 351
        dblVecZiehung = copularnd('Gaussian',cellVecKorrelationsmatrix{1,1},1);
    else
        dblVecZiehung = copularnd('Gaussian',cellVecKorrelationsmatrix{1,2},1);
    end
    
    for ii= 1 : 96
        count = count + 1;
        intSpalte=round(dblVecZiehung(:)*100);
        intSpalte(intSpalte==0) = 1;
        
        dblVecFehlerRnd(count,1) = dblMatFehlerICDF(count,intSpalte(ii));
    end
end


dblVecZeitreihe = dblVecMuster_WP + dblVecFehlerRnd;


end