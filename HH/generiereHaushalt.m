function [ dblVecHaushalt ] = generiereHaushalt(dblMatHaushalte, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster, cellVecKorrelationsmatrix_WP, dblMatFehlerICDF_WP, dblVecMuster_WP)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
dblVecHaushalt = erstelleZeitreihe(true, korrelationsmatrix, dblMatFehlerICDF, dblVecMuster);
dblVecHaushalt = dblVecHaushalt / sum(dblVecHaushalt);
dblVecHaushalt = dblVecHaushalt * dblMatHaushalte(1,1) * 4;

if dblMatHaushalte(1,5) > 0 % Wärmepumpen vorhanden
    dblVecWP = erstelleZeitreiheWP(cellVecKorrelationsmatrix_WP, dblMatFehlerICDF_WP, dblVecMuster_WP);
    dblVecWP = dblVecWP * dblMatHaushalte(1,5)/max(dblVecWP);
    
    dblVecHaushalt = dblVecHaushalt + dblVecWP;
end
end
