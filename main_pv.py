import gsee
import os

#import solar global, solar direct and temperature from SQL
#1.input = PLZ ID, 2.input = year
[db_data, lat, long] = gsee.connect_sql.get_sql_data(240352,10)

#simulate power generation from pv
result = gsee.pv.run_model(
    db_data,
    coords=(lat, long),     # Latitude and longitude
    tilt=30,                # 30 degrees tilt angle
    azim=180,               # facing towards equator
    tracking=0,             # fixed - no tracking
    capacity=1000,          # 1000W
)

print(lat,long)
path = os.getcwd()
result.to_csv(path + '\\data_export\\PV_output.csv', sep=';')